\documentclass[11pt]{article}


\usepackage{fancyhdr}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{enumerate}
\usepackage[greek,english]{babel}
\usepackage{framed}
\usepackage{listings}

\usepackage{tikz}
\usepackage{pgfplots}
\usetikzlibrary{automata,positioning}
\usepackage[applemac]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{arydshln}
\usepackage{amsmath}

\pagestyle{fancy}

\setlength{\parindent}{0cm}

\usepackage{titlesec}
%\newcommand{\sectionbreak}{\clearpage}
\newcommand{\comment}[1]{}

\usepackage{listings}
\usepackage{xcolor}

\definecolor{commentsColor}{rgb}{0.497495, 0.497587, 0.497464}
\definecolor{keywordsColor}{rgb}{0.000000, 0.000000, 0.635294}
\definecolor{stringColor}{rgb}{0.558215, 0.000000, 0.135316}


\lstset{ %
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{commentsColor}\textit,    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  frame=tb,	                   	   % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{keywordsColor}\bfseries,       % keyword style
  language=Python,                 % the language of the code (can be overrided per snippet)
  otherkeywords={*,...},           % if you want to add more keywords to the set
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{commentsColor}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=1,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{stringColor}, % string literal style
  tabsize=2,	                   % sets default tabsize to 2 spaces
  title=\lstname,                  % show the filename of files included with \lstinputlisting; also try caption instead of title
  columns=fixed                    % Using fixed column width (for e.g. nice alignment)
}

\lstdefinelanguage{XML}
{
  morestring=[b]",
  morestring=[s]{>}{<},
  morecomment=[s]{<?}{?>},
  stringstyle=\color{stringColor},
  identifierstyle=\color{keywordsColor},
  keywordstyle=\color{cyan},
  morekeywords={xmlns,version,type, id, idNr, out}% list your attributes here
}


\title{Code Generation (Petri nets + Python)}
\author{Mathias Beke}
\date{\today}


\begin{document}


\lhead{Model Driven Engineering}
\rhead{Mathias Beke - Kristof De Middelaer}

	\begin{center}

	   \vspace*{0.5 cm}

	   \Large\textbf{Code Generation (Petri nets + Python)}\\

	   \vspace*{0.5 cm}

	   \Large{Mathias Beke - Kristof De Middelaer}\\
	   \emph{20120536 - 20121196}


	\end{center}


\vspace*{1.5 cm}

\tableofcontents


\pagebreak

\setcounter{section}{-1}

\section{Source files}

The \emph{mde} folder contains all our AtomPM files (including assets for the concrete syntax). Those files should be placed in your user folder. (Sending the files separately would mean we have to send the assets manually, and specify all exact locations where they need to be placed; we removed some files and folders to shrink it).\\

Additionally we have the \emph{exported{\textunderscore}to{\textunderscore}md} folder which contains all the files to generate the CPNTools and Python files (and the example generated files). Note that Python files must be placed in the \emph{Railway} folder in order to execute them.


\section{CPNTools}
\subsection{Generation of the .cpn file}
In order to get a general idea of how a .cpn file was structured we created a very simple petri net in CPN Tools and looked at the saved file.
We copied the file and replaced the places, transitions and arcs by EGL code that generated these. An issue we had to solve was that we had to generate unique IDs.
We got the ID by taking a substring of the metadepth object names. 

\begin{lstlisting}[language=Xml]
<place id=[%
var idNr : Integer;
idNr = place.asString().substring(6).asInteger();
out.print("'ID".concat(idNr.asString()).concat("'"));
%]>
\end{lstlisting}

The arcs were created by looping over all instances of \textit{Link} which is a parent class of \textit{T2P} and \textit{P2T}.
In order to check whether we were dealing with an arc of \textit{T2P} or \textit{P2T} was just a matter of using the \textit{isTypeOf({\textunderscore})} method.

\begin{lstlisting}
orientation=[% 
if (arc.isTypeOf(T2P)) {
 out.print("'TtoP'");
}
else {
 out.print("'PtoT'");
}
%]
\end{lstlisting}

\subsection{Generation of the ML query}
We had to check whether or not the two trains could be scheduled in a fully independent way: the trains can arrive in their endstations and no segment was entered twice.
We use the Mark facility to check the markings of specific places in the nodes of the reachability graph.
As mentioned in the assignment: it is not possible to check the markings of all places without knowing their name.
\\ \\
Needless to say: it would be very user unfriendly if one would have to manually create the query, look at all the nodes involved and copy/paste them.
For this reason we let the query be generated using EGL. This way the user only needs to run metadepth with the suitable commandlist.
\\ \\
An example of a query:
\begin{lstlisting}
SearchNodes (
             EntireGraph,
			 fn n => (
(length(Mark.exported'Antwerpen76 1 n) < 2) andalso
(length(Mark.exported'Gent78 1 n) < 2) andalso
(length(Mark.exported'visited81 1 n) < 2) andalso
(length(Mark.exported'visited88 1 n) < 2) andalso
(length(Mark.exported'junction95 1 n) < 2) andalso
(length(Mark.exported'visitedJunction96 1 n) < 2) andalso
(length(Mark.exported'visited106 1 n) < 2) andalso
(length(Mark.exported'visited114 1 n) < 2) andalso
(length(Mark.exported'down121 1 n) < 2) andalso
(length(Mark.exported'visitedDown122 1 n) < 2) andalso
(length(Mark.exported'visitUp123 1 n) < 2) andalso
(length(Mark.exported'up124 1 n) < 2) andalso
(length(Mark.exported'endStation135 1 n) < 2) andalso
(length(Mark.exported'visitedEndStation136 1 n) < 2) andalso
(length(Mark.exported'endStation143 1 n) < 2) andalso
(length(Mark.exported'visitedEndStation144 1 n) < 2) andalso
(length(Mark.exported'visitedEndStation136 1 n) > 0) andalso
(length(Mark.exported'visitedEndStation144 1 n) > 0)
),
			  NoLimit,
			  fn n => n,
			  [],
			  op ::)
\end{lstlisting}

\subsection{Workflow for checking the condition}
\begin{enumerate}
	\item Create your railway model in AtomPM
	\item Run the T{\textunderscore}R2PN transformation on the model
	\item Export the generated Petri Net to metadepth using the export toolbar
	\item Generate the .cpn file from the metadepth file by running metadepth with the proper commandlist file
	\item Generate the query file from the metadepth file by running metadepth with the proper commandlist file
	\item Open CPN Tools and import the generated .cpn file
	\item Set the initmarkings
	\item Calculate the state space
	\item Copy the generated query to an aux text field (delete the last andalso in the query)
	\item Evaluate the query as an ML expression
\end{enumerate}

\subsection{Examples}
\subsubsection{largetest2}
The used railway model here is largetest2.model.

\includegraphics[width=\textwidth]{largetest2.png}

The generated Petri Net can be found in largeTest2PN.model. 
When exported to metadepth and giving it the name \textit{exported} we can run metadepth using \textit{commandlist{\textunderscore}cpntools}.
The exported.cpn file will be generated. Running metadepth with \textit{commandlist{\textunderscore}exportedquery} will generate the exported{\textunderscore}query.ml file. 
Executing the query yields the following result:

\includegraphics[width=\textwidth]{cpntools2.png}

There is a way to fully independently schedule the trains!
\subsubsection{ex5Test2}
The used railway model here is Ex5Test2.model.

\includegraphics[width=\textwidth]{test.png}

The generated Petri Net can be found in Ex5Test2PN.model.
When exported to metadepth and giving it the name \textit{ex5Test} we can run metadepth using \textit{commandlist{\textunderscore}cpntools}.
The ex5Test.cpn file will be generated. Running metadepth with \textit{commandlist{\textunderscore}ex5query} will generate the ex5{\textunderscore}query.ml file. 
Executing the query yields the following result:

\includegraphics[width=\textwidth]{cpntools.png}

There is NO way to fully independently schedule the trains!

\section{Python}

\subsection{Code generation}

To generate the Python code we started by creating all the segments. E.g. the instances of stations.

\begin{lstlisting}
# Stations
[% for (station in Station.getAllInstances()) { %]
[%=station.asString()%] = Station([%=station.position[0]%],[%=station.position[1]%], "[%=station.name%]")
[% } %]
\end{lstlisting}

Creating the lights and adding them to the segments:

\begin{lstlisting}
# Lights
[% for (light in Light.getAllInstances()) { %]
[%=light.asString()%] = Light([%=(light.color == "GREEN").asString().firstToUpperCase()%])
[% } %]

# Add lights to segments
[% for (light in lightOfTrack.getAllInstances()) { %]
[%=light.dst.asString()%].setLight([%=light.src.asString()%])
[% } %]
\end{lstlisting}


Then made all the connections (and the special cases of connections like junctions and turnouts).

\begin{lstlisting}
# Connect all segments
# -> regular segment
[% for (cS in connectSegment.getAllInstances()) { %]
[%=cS.dst.asString()%].setNext([%=cS.src.asString()%])
[% } %]

# -> all possible connections to junction-turnout
[% for (cS in TopUnder.getAllInstances()) { %]
[%=cS.dst.asString()%].setNextDiverging([%=cS.src.asString()%]) # TopUnder
[% } %]
[% for (cS in TopTop.getAllInstances()) { %]
[%=cS.dst.asString()%].setNext([%=cS.src.asString()%]) # TopTop
[% } %]
[% for (cS in connectSegmentJUnder.getAllInstances()) { %]
[%=cS.dst.asString()%].setNext([%=cS.src.asString()%]) # connectSegmentJUnder
[% } %]
[% for (cS in UnderUnder.getAllInstances()) { %]
[%=cS.dst.asString()%].setNextDiverging([%=cS.src.asString()%]) # UnderUnder
[% } %]
[% for (cS in UnderTop.getAllInstances()) { %]
[%=cS.dst.asString()%].setNext([%=cS.src.asString()%]) # UnderTop
[% } %]
[% for (cS in connectSegmentTUnder.getAllInstances()) { %]
[%=cS.dst.asString()%].setNextDiverging([%=cS.src.asString()%]) # connectSegmentTUnder
[% } %]
\end{lstlisting}

Adding the segments to the railway:

\begin{lstlisting}
# Add all segments to the railway
[% for (segment in Segment.getAllInstances()) { %]
railway.addSegment([%=segment.asString()%])
[% } %]
\end{lstlisting}

The complete the code generation we also needed to add the trains, and to place them in their start stations.

\begin{lstlisting}
# Trains
[% for (train in Train.getAllInstances()) { %]
[%=train.asString()%] = Train("[%=train.name%]")
[%
for (ss in StartStep.getAllInstances()) {
%]
[%
	if (ss.train == train.name) {
		for (station in Station.getAllInstances()) {
			if (station.name == ss.station) {
%]
[%=train.asString()%].setSegment([%=station.asString()%])
[%
			}
		}
	}
}
%]
railway.addTrain([%=train.asString()%])
[% } %]
\end{lstlisting}



\subsection{Tests}

We added two test files: \emph{smalltest} and \emph{largetest2}. Especially the second one is interesting since it covers most of the cases:

\includegraphics[width=\textwidth]{largetest2.png}

The generated Python code can be found in \emph{exported{\textunderscore}largetest2.py}.\\

We first had trouble running everything on a Mac, because the Python window stayed blank. We then tested on Windows which (after resizing the window) gave us the correct results.\\

Since we were using our own positionable in the AtomPM model -- which fits our own icons/design -- the Python output of the model doesn't make much sense. (But it is quite understandable.)

\includegraphics[width=\textwidth]{largetest2python.png}

\end{document}