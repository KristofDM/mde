\documentclass[11pt]{article}


\usepackage{fancyhdr}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{enumerate}
\usepackage[greek,english]{babel}
\usepackage{framed}
\usepackage{listings}

\usepackage{tikz}
\usepackage{pgfplots}
\usetikzlibrary{automata,positioning}
\usepackage[applemac]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{arydshln}
\usepackage{amsmath}

\pagestyle{fancy}

\setlength{\parindent}{0cm}

\usepackage{titlesec}
%\newcommand{\sectionbreak}{\clearpage}
\newcommand{\comment}[1]{}

\usepackage{listings}
\usepackage{xcolor}

\definecolor{commentsColor}{rgb}{0.497495, 0.497587, 0.497464}
\definecolor{keywordsColor}{rgb}{0.000000, 0.000000, 0.635294}
\definecolor{stringColor}{rgb}{0.558215, 0.000000, 0.135316}


\lstset{ %
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{commentsColor}\textit,    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  frame=tb,	                   	   % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{keywordsColor}\bfseries,       % keyword style
  language=Python,                 % the language of the code (can be overrided per snippet)
  otherkeywords={*,...},           % if you want to add more keywords to the set
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{commentsColor}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=1,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{stringColor}, % string literal style
  tabsize=2,	                   % sets default tabsize to 2 spaces
  title=\lstname,                  % show the filename of files included with \lstinputlisting; also try caption instead of title
  columns=fixed                    % Using fixed column width (for e.g. nice alignment)
}


\title{Rule-Based Translational Semantics}
\author{Mathias Beke}
\date{\today}


\begin{document}


\lhead{Model Driven Engineering}
\rhead{Mathias Beke - Kristof De Middelaer}

	\begin{center}

	   \vspace*{0.5 cm}

	   \Large\textbf{Rule-Based Translational Semantics}\\

	   \vspace*{0.5 cm}

	   \Large{Mathias Beke - Kristof De Middelaer}\\
	   \emph{20120536 - 20121196}


	\end{center}


\vspace*{1.5 cm}

\tableofcontents


\pagebreak



\section{Rules}

\subsection{Start}

\emph{File: R{\textunderscore}PNStart.model}

We match a train, a station and a start step in the schedule. We check if the names match:

\begin{lstlisting}
result = (getAttr("train", "0") == getAttr("name", "1") and getAttr("station", "0") == getAttr("name", "2"))
\end{lstlisting}

If they match we create a petri net place.\\


\includegraphics[width=\textwidth]{pn_start.png}

\subsection{General segment}

\emph{File: R{\textunderscore}S2S.model}

For the regular segments, we create a new place for the next segment (and naturally a link to it from the current segment).\\
We also create another place, we use to trace the behavior of the system. That way we can check if a segment is visited and how many times it was visited. (This extra place is also used in the other rules, so we won't discuss it further) 

\includegraphics[width=\textwidth]{pn_s2s.png}


\subsection{Station}

\emph{File: R{\textunderscore}PNStation.model}

The rule for the station is quite analogues to the general segment rule. We only have a different NAC. This is so that when a station arrives at its endstation it doesn't keep going.

\includegraphics[width=\textwidth]{pn_station.png}

\subsection{Turnout and Junction}

\emph{File: R{\textunderscore}PNTurnout.model}\\
For the turnout we have have a transition to a new place for the respective next segments.\\


\emph{File: R{\textunderscore}PNTurnout2.model}\\
Another rule for the turnout matches a junction and another segment (i.e. NOT from one turnout to one junction).
\begin{lstlisting}
def findNeighbor(left, right):
  neighboringSegmentLinks = getNeighbors("<", "*", left)
  for neighborLink in neighboringSegmentLinks:
    neighboringSegments = getNeighbors("<", "*", neighborLink)
    for neighbor in neighboringSegments:
      if neighbor == right:
        return True
  return False

result = (findNeighbor("0", "1") and findNeighbor("0", "4"))
\end{lstlisting}


\emph{File: R{\textunderscore}PNJunction.model}\\
For the regular junction we have a transition from two places to one place (i.e. the junction).\\

\emph{File: R{\textunderscore}PNT2J.model}\\
We match an association between exactly one turnout and one junction.
For this rule we have a just one transition from the place of the turnout to the place of the junction.
\begin{lstlisting}
def findNeighbor(left, right):
  neighboringSegmentLinks = getNeighbors("<", "*", left)
  for neighborLink in neighboringSegmentLinks:
    neighboringSegments = getNeighbors("<", "*", neighborLink)
    for neighbor in neighboringSegments:
      if neighbor == right:
        return True
  return False

# We know that the only neighbor of the turnout is the junction
result = (findNeighbor("0", "1") and not findNeighbor("0", "4"))
\end{lstlisting}



\subsection{End check}

\emph{File: R{\textunderscore}PNEndCheck.model}

In the end check we match a station with a place and a end step in a schedule.
We then check if the names match:

\begin{lstlisting}
result = (getAttr("station", "0") == getAttr("name", "1"))
\end{lstlisting}
In the RHS we add another link to prevent from matching again in future iterations.\\


\includegraphics[width=\textwidth]{pn_endstep.png}


\subsection{MoTiF Transformation}
The MoTiF transformation begins with the Start rule. This matches all trains and their StartStep to their begin stations. It creates the initial Petri Net place with 1 token.

Then we have a Station rule because the first move will be from a station. We start the loop which goes through all rules. The \textit{tricky} part was to know when the complete transformation was finished. This was solved by creating the same sequence of rules. If the complete sequence of rules has been finished without any of them succeeding we consider the transformation finished.

\emph{File: T{\textunderscore}R2PN.model}

\includegraphics[width=\textwidth]{pn_motif.png}

\section{Test}

A test model can be found in the \emph{R2PNTest.model} file. It contains two trains with a start and end station and creates the correct petri net. The railway system contains turnouts, junctions, turnouts to junctions etc.

\includegraphics[width=\textwidth]{R2PNTest.png}
\end{document}