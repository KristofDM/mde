class MiniDevsGrammar(object):
	def __init__(self):
		self.tokens = {
			'condition_tok': {'type': 'token', 'reg' : 'condition', 'errortext': 'condition'},
			'action_tok': {'type': 'token', 'reg' : 'action', 'errortext': 'action'},
			'with_tok': {'type': 'token', 'reg' : 'with', 'errortext': 'with'},
			'ta_tok': {'type': 'token', 'reg' : 'time advance', 'errortext': 'time advance'},
			'transfer_tok': {'type': 'token', 'reg' : 'transfer', 'errortext': 'transfer'},
			'internal_tok': {'type': 'token', 'reg' : 'internal', 'errortext': 'internal'},
			'external_tok': {'type': 'token', 'reg' : 'external', 'errortext': 'external'},
			'confluent_tok': {'type': 'token', 'reg' : 'confluent', 'errortext': 'confluent'},
			'transition_tok': {'type': 'token', 'reg' : '->', 'errortext': 'transition'},
			'out_tok': {'type': 'token', 'reg' : 'output', 'errortext': 'output'},
			'connections_tok': {'type': 'token', 'reg' : 'connections', 'errortext': 'connections'},
			'from': {'type': 'token', 'reg' : 'from', 'errortext': 'from'},
			'to': {'type': 'token', 'reg' : 'to', 'errortext': 'to'},

			'lbrac': {'type': 'token', 'reg' : '{', 'errortext': '{'},
			'rbrac': {'type': 'token', 'reg' : '}', 'errortext': '}'},
			'lsqp': {'type': 'token', 'reg' : '\[', 'errortext': '['},
			'rsqp': {'type': 'token', 'reg' : '\]', 'errortext': ']'},

			'lpar': {'type': 'token', 'reg' : '\(', 'errortext': 'left parenthesis'},
			'rpar': {'type': 'token', 'reg' : '\)', 'errortext': 'right parenthesis'},


			'sum_tok': {'type': 'token', 'reg' : '\+', 'errortext': '+'},
			'sub_tok': {'type': 'token', 'reg' : '-', 'errortext': '-'},
			'mult_tok': {'type': 'token', 'reg' : '\*', 'errortext': '*'},
			'div_tok': {'type': 'token', 'reg' : '/', 'errortext': '/'},

 			'decimal': {'type': 'token', 'reg' : '[+-]?(0|[1-9]\d*[lL]?)', 'errortext': 'decimal'},
 			'float': {'type': 'token', 'reg' : '[+-]?((\d+\.\d*|\.\d+)([eE][-+]?\d+)?|\d+[eE][-+]?\d+)', 'errortext': 'float'},

 			'strvalue': {'type': 'token', 'reg' : r'u?r?("(?!"").*?(?<!\\)(\\\\)*?"|\'(?!\'\').*?(?<!\\)(\\\\)*?\')', 
						'errortext': 'string value'},
 			'long_strvalue': {'type': 'token', 'reg' : r'(?s)u?r?(""".*?(?<!\\)(\\\\)*?"""|\'\'\'.*?(?<!\\)(\\\\)*?\'\'\')', 
						'errortext': 'long string value'},

			'true': {'type': 'token', 'reg' : 'True', 'errortext': 'True value'},
			'false': {'type': 'token', 'reg' : 'False', 'errortext': 'False value'},

 			'None': {'type': 'token', 'reg' : 'None', 'errortext': 'None value'},
 			'Nothing': {'type': 'token', 'reg' : 'nothing', 'errortext': 'nothing value'},

 			#'ext_tok':  {'type': 'token', 'reg' : 'ext', 'errortext': 'ext'},
 			'if_tok':  {'type': 'token', 'reg' : 'if', 'errortext': 'if'},
 			'else_tok':  {'type': 'token', 'reg' : 'else', 'errortext': 'else'},
 			'return_tok':  {'type': 'token', 'reg' : 'return', 'errortext': 'return'},
 			'initial_tok':  {'type': 'token', 'reg' : 'initial', 'errortext': 'initial'},
 			'state_tok':  {'type': 'token', 'reg' : 'state', 'errortext': 'state'},

			'pass_tok': {'type': 'token', 'reg' : 'pass', 'errortext': 'pass'},

 			'while_tok':  {'type': 'token', 'reg' : 'while', 'errortext': 'while'},


 			'atomic_tok':  {'type': 'token', 'reg' : 'atomic', 'errortext': 'atomic'},
 			'coupled_tok':  {'type': 'token', 'reg' : 'coupled', 'errortext': 'coupled'},
 			'instances_tok':  {'type': 'token', 'reg' : 'instances', 'errortext': 'instances'},

 			'outports_tok':  {'type': 'token', 'reg' : 'outports', 'errortext': 'outports'},
 			'inports_tok':  {'type': 'token', 'reg' : 'inports', 'errortext': 'inports'},
 			'component_tok': {'type': 'token', 'reg' : 'component', 'errortext': 'component'},
 			'statedef_tok': {'type': 'token', 'reg' : 'statedef', 'errortext': 'state definition'},
 			'event_tok': {'type': 'token', 'reg' : 'event', 'errortext': 'event definition'},
 			'top_tok': {'type': 'token', 'reg' : 'top', 'errortext': 'top'},
			'bottom_tok': {'type': 'token', 'reg' : 'bottom', 'errortext': 'bottom'},

 			'dot': {'type': 'token', 'reg' : '\.', 'errortext': 'dot'},
			'name': {'type': 'token', 'reg' : '[a-zA-Z_][a-zA-Z_0-9]*(?!r?"|r?\')', 'errortext': 'name token'},
 			'whitespace': {'type': 'token', 'reg' : '[ ]+', 'errortext': 'white space', 'hidden': True},
			'newline': {'type': 'token', 'reg' : '[ \t]*\n', 'errortext': 'newline', 'hidden': True},
 			'comments': {'type': 'token', 'reg' : '\#[^\n]*\n+', 'errortext': 'comments', 'hidden': True},
			'comma': {'type': 'token', 'reg' : ',', 'errortext': 'comma'},
			'semicolon': {'type': 'token', 'reg' : ';', 'errortext': 'semicolon'},
			'assign': {'type': 'token', 'reg' : '=', 'errortext': 'assign'},
			'equals': {'type': 'token', 'reg' : '==', 'errortext': 'equals'},

			'colon': {'type': 'token', 'reg' : ':', 'errortext': 'colon \':\' token'},

 			'tabspace': {'type': 'token', 'reg' : '\t', 'errortext': 'tab', 'hidden': True},
 			'4ws': {'type': 'token', 'reg' : '    ', 'errortext': 'four spaces', 'hidden': True}
		}
		self.rules = {
			'start': {'type': 'prod', 'body' : 
				['+','@statement'], 'errortext': 'start definition',
				'interleave': ['*', '@ignore']}, 

			'top': {'type': 'prod', 'body' : ['.', '$top_tok','$colon',['+',['#indent','@statement']]]},
			'bottom': {'type': 'prod', 'body' : ['.', '$bottom_tok','$colon',['+',['#indent','@statement']]]},

			'toplevel': {'type': 'prod', 'body' : ['|', '@atomic_component', '@coupled_component', '@statedef', '@event']},

 			'statedef': {'type': 'prod', 'body' : ['.', '$statedef_tok', '$name', 
													'$lpar', ['?','@args'], '$rpar'],
						'errortext': 'state definition'},

 			'event': {'type': 'prod', 'body' : ['.', '$event_tok', '$name', 
													'$lpar', ['?','@args'], '$rpar'],
						'errortext': 'state definition'},

 			'atomic_component': {'type': 'prod', 'body' : ['.', '$atomic_tok', '@comp_name', 
													'$lpar', ['?','@args'], '$rpar', '$with_tok', '@statedef_name',
													['?',['.','$colon',['+',['#indent','@atomic_element']]]]
												],
						'errortext': 'atomic component definition'},

			'comp_name': {'type': 'prod', 'body' : ['.', '$name'], 'errortext': 'component name'},
			'statedef_name': {'type': 'prod', 'body' : ['.', '$name'], 'errortext': 'state definition name'},

 			'coupled_component': {'type': 'prod', 'body' : ['.', '$coupled_tok','@comp_name', 
													'$lpar', ['?','@args'], '$rpar', 
													['.','$colon',['+',['#indent','@coupled_element']]]
												],
						'errortext': 'coupled component definition'},

			'atomic_element': {'type': 'prod', 'body' : ['|', '@port_attribute', '@initial','@state'], 
							'errortext': 'atomic element'},

			'coupled_element': {'type': 'prod', 'body' : ['|', '@port_attribute', '@instances', '@connections'], 
							'errortext': 'coupled element'},

			'statement': {'type': 'prod', 'body' : ['|', '@ifelse', '@return', '@while', '@assignment', '@pass', '@functioncall_stm'], 
							'errortext': 'statement definition'},

			'while': {'type': 'prod', 'body' : ['.', '$while_tok','@expression','$colon',
													['+',['#indent','@statement']]], 'errortext': 'while definition'},

			'pass': {'type': 'prod', 'body' : ['.', '$pass_tok'], 'errortext': 'pass'},

			'return': {'type': 'prod', 'body' : ['.', '$return_tok','@expression'], 'errortext': 'return definition'},

			'ifelse': {'type': 'prod', 'body' : ['.', '$if_tok','@expression','$colon',
													['+',['#indent','@statement']],
													['?',['#(0)indent','$else_tok','$colon',['+',['#indent','@statement']]]]], 
													'errortext': 'if else definition'},

			'connections': {'type': 'prod', 'body' : ['.', '$connections_tok','$colon',
													['+',['#indent','@connections_body']]], 'errortext': 'connections definition'},

			'connections_body': {'type': 'prod', 'body' : ['.', '$from', '@dotted_name', '$to', '@dotted_name', ['?','@transfer']], 
								'errortext': 'connections definition'},

			'transfer': {'type': 'prod', 'body' : ['.', '$colon', ['#indent','@transfer_def']], 
								'errortext': 'transfer case'},

			'transfer_def': {'type': 'prod', 'body' : ['.', '$transfer_tok', '$colon', ['+',['#indent', '@statement']]], 
								'errortext': 'transfer definition'},

			'initial': {'type': 'prod', 'body' : ['.', '$initial_tok', '$name'], 'errortext': 'initial definition'},

 			'pars': {'type': 'prod', 'body' : ['.', '@expression', ['*',['.','$comma','@expression']]],
						'errortext': 'parameters'},

			'state': {'type': 'prod', 'body' : ['.', '$state_tok','$name', 
												'$colon', 
												['+',['#indent', '@state_element']]
												], 'errortext': 'state definition'},

			'state_element': {'type': 'prod', 'body' : ['|', '@timeadvance', '@externaltransition', 
													'@internaltransition', '@confluenttransition', '@outputfunction'], 
						'errortext': 'state body'},

			'timeadvance': {'type': 'prod', 'body' : ['.', '$ta_tok', '$colon', ['+',['#indent', '@statement']]], 
				'errortext': 'time advance function'},


			'outputfunction': {'type': 'prod', 'body' : ['.', '$out_tok', '$colon', ['+',['#indent', '@statement']]], 
				'errortext': 'output function'},

			'internaltransition': {'type': 'prod', 'body' : ['.', '$internal_tok','$transition_tok','$name', 
															['?', ['.', '$colon', '@transition_body']]], 
						'errortext': 'internal transition'},

			'externaltransition': {'type': 'prod', 'body' : ['.', '$external_tok','$transition_tok','$name',
															['?', ['.','$colon', '@transition_body']]], 
						'errortext': 'external transition'},

			'confluenttransition': {'type': 'prod', 'body' : ['.', '$confluent_tok', '$transition_tok', '$name',
															['?', ['.', '$colon', '@transition_body']]], 
						'errortext': 'confluent transition'},

			'transition_body': {'type': 'prod', 'body' : ['|', ['#indent', '@condition'] , ['#indent', '@action']], 
						'errortext': 'transition body'},

			'condition': {'type': 'prod', 'body' : ['.', '$condition_tok','$colon', ['+',['#indent', '@statement']]], 
												'errortext': 'condition definition'},

			'action': {'type': 'prod', 'body' : ['.', '$action_tok','$colon', ['+',['#indent', '@statement']]], 
												'errortext': 'action definition'},

			'assignment': {'type': 'prod', 'body' : ['.', '@expression','$assign','@expression'], 
												'errortext': 'assignment definition'},

			'instances': {'type': 'prod', 'body' : ['.', '$instances_tok','$colon',
													['+',['#indent','@coupled_assignment']]], 'errortext': 'coupled definition'},

			'coupled_assignment': {'type': 'prod', 'body' : ['.', '$name','$assign', '@functioncall'], 
												'errortext': 'coupled assignment definition'},

			'inport_attribute': {'type': 'prod', 'body' : ['.', '$inports_tok', '@name_list'], 
								'errortext': 'inport attribute'},

			'outport_attribute': {'type': 'prod', 'body' : ['.', '$outports_tok', '@name_list'], 
								'errortext': 'outport attribute'},

 			'name_list': {'type': 'prod', 'body' : ['.', '$name', ['*',['.','$comma','$name']]],
						'errortext': 'name list definition'},

			'port_attribute': {'type': 'prod', 'body' : ['|','@outport_attribute','@inport_attribute'],
														'errortext': 'port attribute'},

 			'args': {'type': 'prod', 'body' : ['.', '@argument', ['*',['.','$comma','@argument']]],
						'errortext': 'arguments definition'},

 			'argument': {'type': 'prod', 'body' : ['.', '$name', ['?',['.','$assign','@atomvalue']]],
						'errortext': 'argument definition'},

			'atomvalue': {'type': 'prod', 'body' : ['|', '@number', '@boolean', '@string', '$None', '$Nothing'], 'errortext': 'atom value'},
			
			'expression': {'type': 'prod', 'body' : ['|', '@atomvalue', '@nav_expr', '@operation', '@selection','@dict', '@vect', '@functioncall'], 
						'errortext': 'expression'},

			'operation': {'type': 'prod', 'body' : ['.', '@expression', '@op', '@expression'], 
						'errortext': 'operation'},

			'op': {'type': 'prod', 'body' : ['|', '$equals','$sum_tok', '$sub_tok', '$mult_tok', '$div_tok'], 
						'errortext': 'op symbol'},

			'dict': {'type': 'prod', 'body' : ['.', '$lbrac', ['?','@dict_elems'], '$rbrac'], 
						'errortext': 'dictionary'},

			'dict_elems': {'type': 'prod', 'body' : ['.', '@dict_elem', ['*','$comma', ['?','@ignore'],'@dict_elem']], 
						'errortext': 'dictionary elements'},

			'dict_elem': {'type': 'prod', 'body' : ['.', '@expression', '$colon', '@expression'], 
						'errortext': 'dictionary elements'},

			'selection': {'type': 'prod', 'body' : ['.', '@expression','$lsqp', '@expression', '$rsqp'], 
						'errortext': 'selection'},

			'vect': {'type': 'prod', 'body' : ['.', '$lsqp', ['?','@vect_elems'], '$rsqp'], 
						'errortext': 'vector'},

			'vect_elems': {'type': 'prod', 'body' : ['.', '@expression', ['*','$comma', ['?','@ignore'],'@expression']], 
						'errortext': 'vector elements'},

			'functioncall': {'type': 'prod', 'body' : ['.', '@dotted_name', '$lpar', ['?','@pars'],'$rpar'], 
						'errortext': 'call'},

			'functioncall_stm': {'type': 'prod', 'body' : ['.','@dotted_name', '$lpar', ['?','@pars'],'$rpar'], 
						'errortext': 'call'},


			'number': {'type': 'prod', 'body' : ['|', '$decimal', '$float'], 'errortext': 'number'},

			'string': {'type': 'prod', 'body' : ['|', '$strvalue', '$long_strvalue'], 'errortext': 'string'},

			'boolean': {'type': 'prod', 'body' : ['|', '$true', '$false'], 'errortext': 'boolean value'},

 			'dotted_name': {'type': 'prod', 'body' : ['.', '$name', ['*', ['.','$dot', '$name']]], 'errortext': 'dotted name'},
 			'nav_expr': {'type': 'prod', 'body' : ['.', '$name', ['*', ['.','$dot', '@expression']]], 'errortext': 'navigation'},
 			'indent': {'type': 'prod', 'body' : ['|', '$tabspace', '$4ws'], 'errortext': 'indentation'},
 			'ignore': {'type': 'prod', 'body' : ['|', '$whitespace', '$comments', '$newline'], 'errortext': 'ignore', 'hidden': True}
		}








