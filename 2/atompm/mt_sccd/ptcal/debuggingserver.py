'''
Created on 14-okt.-2014

@author: Simon
'''

import sys
sys.path.append("")
sys.path.append("../pypdevs/src/")

import logging, json, os
from collections import defaultdict
from utils import Utilities as utils
from ptcal import PyTCoreAbstractionLayer
from python_runtime.statecharts_core import *
from minidevscsmodel import MiniDevsGrammar
from hutnparser import Parser
from hutnparser import IgnorePostProcessor
from subprocess import call

class PyTCoreAbstractionLayerWithDebugging(PyTCoreAbstractionLayer):
    DEVS = 1
    SCCD = 2
    CBD = 3

    def __init__(self, aswCommTools, mtwid):
        PyTCoreAbstractionLayer.__init__(self, aswCommTools, mtwid)
        self._m = None
        self._m_atompm = None
        self.incoming = defaultdict(set)
        self.outgoing = defaultdict(set)
        self.formalism = None
    
    def __calc_in_out_rel(self, rel_type):
        for key in self.type_map_keys[rel_type]:
            for e in self._m_atompm['edges']:
                if e['dest'] == key:
                    self.incoming[key].add(e['src'])
                    self.outgoing[e['src']].add(key)
                if e['src'] == key:
                    self.outgoing[key].add(e['dest'])
                    self.incoming[e['dest']].add(key)

    def loadModel(self, m, mms, sn):
        assert self._m == None, 'ptcal.loadModel() should only be called once'
        if m.__class__ != {}.__class__ :
            m = json.loads(m)
        self._m_atompm = m
        model = {}
        if len(m['nodes']) == 0:
            return model
        if "/Formalisms/ParallelDEVS/ParallelDEVS_Runtime" in mms:
            from exported_to_pypdevs.experiment import Root, Controller
            self.formalism = self.DEVS
            self.messages = []
            self.breakpoints = []
            self.model = Root()
            self.controller = Controller(self.model)
            self.controller.start()
            self.controller.addInput(Event("trace", "request", ['trace.txt']))
            self.reply_port = self.controller.addOutputListener("reply")
            ''' look for root, but also build a type_map '''
            self.type_map = defaultdict(list)
            self.type_map_keys = defaultdict(list)
            for key, node in m['nodes'].iteritems():
                self.type_map[node["$type"]].append(node)
                self.type_map_keys[node["$type"]].append(key)
                if node["$type"] == '/Formalisms/ParallelDEVS/ParallelDEVS_Runtime/CoupledDEVS' and node['name']['value'] == 'Root':
                    model['Root'] = node
                    node['$loc'] = 'Root'
                elif node['$type'] == '/Formalisms/ParallelDEVS/ParallelDEVS_Debug/GlobalBreakpoint':
                    self.breakpoints.append(key)
                    condition = '\n'.join(map(lambda l: '\t%s' % l, node['condition']['value'].split("\n")))
                    bp_f = 'def breakpoint(time, model, transitioned):\n\ttime = time[0]\n%s' % condition
                    self.controller.addInput(Event("add_breakpoint", "request", [key, bp_f, node['enabled']['value'], node['disable_after_trigger']['value']]))
                node['$key'] = key
            if 'Root' in model:
                # ParallelDEVS Runtime
                ''' find all incoming and outgoing elements '''
                self.__calc_in_out_rel('/Formalisms/ParallelDEVS/ParallelDEVS_Runtime/submodels')
                self.__calc_in_out_rel('/Formalisms/ParallelDEVS/ParallelDEVS_Runtime/states')
                self.__calc_in_out_rel('/Formalisms/ParallelDEVS/ParallelDEVS_Runtime/state')
                self.__calc_in_out_rel('/Formalisms/ParallelDEVS/ParallelDEVS_Runtime/ports')
                self.__calc_in_out_rel('/Formalisms/ParallelDEVS/ParallelDEVS_Debug/target')
                self.__calc_in_out_rel('/Formalisms/ParallelDEVS/ParallelDEVS_Runtime/injectLocation')
                model_queue = [model['Root']]
                while len(model_queue) > 0:
                    curr_model = model_queue.pop()
                    for o_rel_key in filter(lambda x: m['nodes'][x]['$type'] == '/Formalisms/ParallelDEVS/ParallelDEVS_Runtime/submodels', self.outgoing[curr_model['$key']]):
                        submodel = m['nodes'][next(iter(self.outgoing[o_rel_key]))]
                        submodel['$loc'] = curr_model['$loc'] + '.' + submodel['name']['value']
                        model[submodel['$loc']] = submodel
                        model_queue.append(submodel)
                    for o_rel_key in filter(lambda x: m['nodes'][x]['$type'] == '/Formalisms/ParallelDEVS/ParallelDEVS_Runtime/states', self.outgoing[curr_model['$key']]):
                        state = m['nodes'][next(iter(self.outgoing[o_rel_key]))]
                        state['$loc'] = curr_model['$loc'] + '.' + state['name']['value']
                        model[state['$loc']] = state
                        model.setdefault(curr_model['$loc'] + '.' + '$states', []).append(state)
                    for o_rel_key in filter(lambda x: m['nodes'][x]['$type'] == '/Formalisms/ParallelDEVS/ParallelDEVS_Runtime/ports', self.outgoing[curr_model['$key']]):
                        port = m['nodes'][next(iter(self.outgoing[o_rel_key]))]
                        port['$loc'] = curr_model['$loc'] + '.' + port['name']['value']
                        model[port['$loc']] = port
                    if curr_model['$type'] == '/Formalisms/ParallelDEVS/ParallelDEVS_Runtime/AtomicDEVS':
                        for o_rel_key in filter(lambda x: m['nodes'][x]['$type'] == '/Formalisms/ParallelDEVS/ParallelDEVS_Runtime/state', self.outgoing[curr_model['$key']]):
                            state = m['nodes'][next(iter(self.outgoing[o_rel_key]))]
                            state['$loc'] = curr_model['$loc'] + '.' + '$state'
                            model[state['$loc']] = state
                if '/Formalisms/ParallelDEVS/ParallelDEVS_Debug/Breakpoint' in self.type_map_keys:
                    for key in self.type_map_keys['/Formalisms/ParallelDEVS/ParallelDEVS_Debug/Breakpoint']:
                        node = m['nodes'][key]
                        self.breakpoints.append(key)
                        condition = '\n'.join(map(lambda l: '\t%s' % l, node['condition']['value'].split("\n"))) if len(node['condition']['value']) > 0 else '\treturn True'
                        state_id = next(iter(self.outgoing[next(iter(self.outgoing[key]))]))
                        state = self._m_atompm['nodes'][state_id]
                        bp_f = 'def breakpoint(time, model, transitioned):\n\ttime = time[0]\n\tif len(filter(lambda x: x.getModelFullName() == "%s" and x.state.name == "%s", transitioned)) == 0:\n\t\treturn False\n%s' % (state['$loc'][:state['$loc'].rfind(".")], state['name']['value'], condition)
                        self.controller.addInput(Event("add_breakpoint", "request", [key, bp_f, node['enabled']['value'], node['disable_after_trigger']['value']]))
                if '/Formalisms/ParallelDEVS/ParallelDEVS_Runtime/EventInstance' in self.type_map_keys: 
                    for key in self.type_map_keys['/Formalisms/ParallelDEVS/ParallelDEVS_Runtime/EventInstance']:
                        node = m['nodes'][key]
                        self.messages.append(key)
                        port_id = next(iter(self.outgoing[next(iter(self.outgoing[key]))]))
                        port = self._m_atompm['nodes'][port_id]
                        self.controller.addInput(Event("inject", "request", [{"port": port['$loc'], "event": eval(node['event_type']['value'] + '(' + ','.join(map(lambda x: '%s = %s' % (x['name'], x['val']), node['attribute_values']['value'])) + ')')}]))
                model['$simulation'] = self.type_map['/Formalisms/ParallelDEVS/ParallelDEVS_Runtime/SimulationInstance'][0]
                self._m = model
                utils.setTimeout(0.1, self._simulate)
            else:
                self.__calc_in_out_rel('/Formalisms/ParallelDEVS/ParallelDEVS/InternalTransition')
                self.__calc_in_out_rel('/Formalisms/ParallelDEVS/ParallelDEVS/ExternalTransition')
                self.__calc_in_out_rel('/Formalisms/ParallelDEVS/ParallelDEVS/ConfluentTransition')
                self.__calc_in_out_rel('/Formalisms/ParallelDEVS/ParallelDEVS/channel')
        elif "/Formalisms/SCCD/SCCD" in mms:
            from exported_to_sccdxml.model import ExecutorFactory
            self.formalism = self.SCCD
            self.messages = []
            self.breakpoints = []
            self.controller = ExecutorFactory().create()
            self.reply_port = self.controller.addOutputListener('reply')
            self.type_map = defaultdict(list)
            self.type_map_keys = defaultdict(list)
            for key, node in m['nodes'].iteritems():
                self.type_map[node["$type"]].append(node)
                self.type_map_keys[node["$type"]].append(key)
                if node["$type"] == '/Formalisms/SCCD/SCCD/CompositeState' and node['name']['value'] == 'Root':
                    model['Root'] = node
                    node['$loc'] = 'Root'
                elif node['$type'] == '/Formalisms/SCCD/SCCD_Debug/GlobalBreakpoint':
                    self.breakpoints.append(key)
                    condition = '\n'.join(map(lambda l: '\t%s' % l, node['condition']['value'].split("\n")))
                    bp_f = 'def breakpoint(time, state):\n%s' % condition
                    self.controller.addInput(Event("add_breakpoint", "request", [key, bp_f, node['enabled']['value'], node['disable_after_trigger']['value']]))
                node['$key'] = key
            if 'Root' in model:
                model['$clz'] = self.type_map['/Formalisms/SCCD/SCCD/Class'][0]
                ''' find all incoming and outgoing elements '''
                self.__calc_in_out_rel('/Formalisms/SCCD/SCCD/contain')
                self.__calc_in_out_rel('/Formalisms/SCCD/SCCD/containOC')
                self.__calc_in_out_rel('/Formalisms/SCCD/SCCD/ocContain')
                self.__calc_in_out_rel('/Formalisms/SCCD/SCCD/includes')
                self.__calc_in_out_rel('/Formalisms/SCCD/SCCD_Debug/target')
                state_queue = [model['Root']]
                while len(state_queue) > 0:
                    curr_state = state_queue.pop()
                    if curr_state["$type"] == '/Formalisms/SCCD/SCCD/CompositeState':
                        for o_rel_key in filter(lambda x: m['nodes'][x]['$type'] in ('/Formalisms/SCCD/SCCD/contain', '/Formalisms/SCCD/SCCD/includes', '/Formalisms/SCCD/SCCD/containOC'), self.outgoing[curr_state['$key']]):
                            state = m['nodes'][next(iter(self.outgoing[o_rel_key]))]
                            state['$loc'] = curr_state['$loc'] + '_' + state['name']['value']
                            model[state['$loc']] = state
                            if state['$type'] in ('/Formalisms/SCCD/SCCD/CompositeState', '/Formalisms/SCCD/SCCD/OrthogonalComponent'):
                                state_queue.append(state)
                    elif curr_state["$type"] == '/Formalisms/SCCD/SCCD/OrthogonalComponent':
                        for o_rel_key in filter(lambda x: m['nodes'][x]['$type'] == '/Formalisms/SCCD/SCCD/ocContain', self.outgoing[curr_state['$key']]):
                            state = m['nodes'][next(iter(self.outgoing[o_rel_key]))]
                            state['$loc'] = curr_state['$loc'] + '_' + state['name']['value']
                            model[state['$loc']] = state
                            if state['$type'] in ('/Formalisms/SCCD/SCCD/CompositeState', '/Formalisms/SCCD/SCCD/OrthogonalComponent'):
                                state_queue.append(state)
                if '/Formalisms/SCCD/SCCD_Debug/Breakpoint' in self.type_map_keys:
                    for key in self.type_map_keys['/Formalisms/SCCD/SCCD_Debug/Breakpoint']:
                        node = m['nodes'][key]
                        self.breakpoints.append(key)
                        condition = '\n'.join(map(lambda l: '\t%s' % l, node['condition']['value'].split("\n"))) if len(node['condition']['value']) > 0 else '\treturn True'
                        state_id = next(iter(self.outgoing[next(iter(self.outgoing[key]))]))
                        state = self._m_atompm['nodes'][state_id]
                        bp_f = 'def breakpoint(time, state):\n\tstates=set([])\n\tfor v in state.itervalues():\n\t\tstates.update(v)\n\tif not "%s" in states:\n\t\treturn False\n%s' % (state['$loc'], condition)
                        self.controller.addInput(Event("add_breakpoint", "request", [key, bp_f, node['enabled']['value'], node['disable_after_trigger']['value']]))
                model['$simulation'] = self.type_map['/Formalisms/SCCD/SCCD/SimulationInstance'][0]
                self._m = model
                utils.setTimeout(0.1, self._simulate)
        elif "/Formalisms/CBDs/CBD_Runtime" in mms:
            import exported_to_cbd.model
            reload(exported_to_cbd.model)
            from exported_to_cbd.model import Root, numTimeSteps
            from exported_to_cbd.CBDMultipleOutput.Source.cbd_sccd import Controller
            self.formalism = self.CBD
            self.breakpoints = []
            self.to_graph = {}
            Root.setClock(None)
            self.controller = Controller(Root)
            self.controller.start()
            self.reply_port = self.controller.addOutputListener('reply')
            self.type_map = defaultdict(list)
            self.type_map_keys = defaultdict(list)
            for key, node in m['nodes'].iteritems():
                self.type_map[node["$type"]].append(node)
                self.type_map_keys[node["$type"]].append(key)
                if node["$type"] == '/Formalisms/CBDs/CBD_Runtime/CBD' and node['name']['value'] == 'Root':
                    model['Root'] = node
                    node['$loc'] = 'Root'
                node['$key'] = key
            if 'Root' in model:
                ''' find all incoming and outgoing elements '''
                self.__calc_in_out_rel('/Formalisms/CBDs/CBD_Runtime/contents')
                self.__calc_in_out_rel('/Formalisms/CBDs/CBD_Runtime/rcontents')
                self.__calc_in_out_rel('/Formalisms/CBDs/CBD_Runtime/cbd_ports')
                self.__calc_in_out_rel('/Formalisms/CBDs/CBD_Runtime/IN')
                self.__calc_in_out_rel('/Formalisms/CBDs/CBD_Runtime/LeftOperand')
                self.__calc_in_out_rel('/Formalisms/CBDs/CBD_Runtime/RightOperand')
                self.__calc_in_out_rel('/Formalisms/CBDs/CBD_Runtime/IN_N')
                self.__calc_in_out_rel('/Formalisms/CBDs/CBD_Runtime/IC')
                self.__calc_in_out_rel('/Formalisms/CBDs/CBD_Runtime/Delay_IN')
                self.__calc_in_out_rel('/Formalisms/CBDs/CBD_Runtime/deltaT_der')
                self.__calc_in_out_rel('/Formalisms/CBDs/CBD_Runtime/deltaT_int')
                self.__calc_in_out_rel('/Formalisms/CBDs/CBD_Debug/target')
                cbd_queue = [model['Root']]
                self.intder = set([])
                while len(cbd_queue) > 0:
                    curr_cbd = cbd_queue.pop()
                    for o_rel_key in filter(lambda x: m['nodes'][x]['$type'] in ('/Formalisms/CBDs/CBD_Runtime/contents', '/Formalisms/CBDs/CBD_Runtime/rcontents', '/Formalisms/CBDs/CBD_Runtime/cbd_ports'), self.outgoing[curr_cbd['$key']]):
                        block = m['nodes'][next(iter(self.outgoing[o_rel_key]))]
                        if not block['$type'] in ('/Formalisms/CBDs/CBD_Runtime/InputPort', '/Formalisms/CBDs/CBD_Runtime/OutputPort'):
                            block['$loc'] = curr_cbd['$loc'] + '.' + block['name']['value']
                            model[block['$loc']] = block
                            if block['$type'] == '/Formalisms/CBDs/CBD_Runtime/CBD':
                                cbd_queue.append(block)
                            elif block['$type'] in ('/Formalisms/CBDs/CBD_Runtime/IntegratorBlock', '/Formalisms/CBDs/CBD_Runtime/DerivatorBlock'):
                                self.intder.add(block['$loc'])
                    for o_rel_key in filter(lambda x: m['nodes'][x]['$type'] == '/Formalisms/CBDs/CBD_Runtime/cbd_ports', self.outgoing[curr_cbd['$key']]):                        
                        block = m['nodes'][next(iter(self.outgoing[o_rel_key]))]
                        block['$loc'] = curr_cbd['$loc'] + '.' + block['name']['value']
                        model[block['$loc']] = block
                if '/Formalisms/CBDs/CBD_Debug/Breakpoint' in self.type_map_keys:
                    for key in self.type_map_keys['/Formalisms/CBDs/CBD_Debug/Breakpoint']:
                        node = m['nodes'][key]
                        self.breakpoints.append(key)
                        block_id = next(iter(self.outgoing[next(iter(self.outgoing[key]))]))
                        block = self._m_atompm['nodes'][block_id]
                        self.controller.addInput(Event("add_breakpoint", "request", [key, block['$loc'][5:], node['enabled']['value'], node['disable_after_trigger']['value']]))
                if '/Formalisms/CBDs/CBD_Runtime/Plot' in self.type_map_keys:
                    for key in self.type_map_keys['/Formalisms/CBDs/CBD_Runtime/Plot']:                        
                        node = m['nodes'][key]
                        self.to_graph.setdefault(node['yblockname']['value'], []).append((node['$key'], node['xblockname']['value']))
                model['$simulation'] = self.type_map['/Formalisms/CBDs/CBD_Runtime/SimulationInfo'][0]
                self._m = model
                utils.setTimeout(0.1, self._simulate)
        else:
            print 'unknown formalism %s' % mms
        
    def loadTransforms(self, fnames):
        pass

    def _parseFunction(self, fdef):
        result = Parser(MiniDevsGrammar()).parse(fdef)
        if (result['status'] != Parser.Constants.Success):
            return result['text']
        else:
            return 'success'

    def validate(self):
        if self.formalism == self.DEVS:
            self._requestUnhighlight()
            reqs = []
            if '/Formalisms/ErrorMessage/ErrorMessage/ErrorMessage' in self.type_map_keys:
                for nid in self.type_map_keys['/Formalisms/ErrorMessage/ErrorMessage/ErrorMessage']:
                    reqs.append(self._removeNode('/Formalisms/ErrorMessage/ErrorMessage/ErrorMessage/' + str(nid) + '.instance'))
            def _validateNodes(ntype, functions):
                for nid in self.type_map_keys[ntype]:
                    n = self._m_atompm['nodes'][nid]
                    for func in functions:
                        if n[func]['value'] != '':
                            res = self._parseFunction(n[func]['value'])
                            if res != 'success':
                                print res
                                self._requestNodeHighlightWithID(nid, 'red')
                                atompm_id = '$%s$' % len(reqs)
                                reqs.append(self._createNode('/Formalisms/ErrorMessage/ErrorMessage/ErrorMessage'))
                                pos = n['position']['value'] if 'position' in n else self._m_atompm['nodes'][next(iter(self.incoming[nid]))]['position']['value']
                                reqs.append(self._changeAttrWithID('/Formalisms/ErrorMessage/ErrorMessage/ErrorMessage/' + atompm_id, 'position', pos))
                                reqs.append(self._changeAttrWithID('/Formalisms/ParallelDEVS/ParallelDEVS_Runtime/EventInstance/' + atompm_id, 'message', res))
            
            _validateNodes('/Formalisms/ParallelDEVS/ParallelDEVS/State', ['output', 'time_advance'])
            _validateNodes('/Formalisms/ParallelDEVS/ParallelDEVS/InternalTransition', ['condition', 'action'])
            _validateNodes('/Formalisms/ParallelDEVS/ParallelDEVS/ExternalTransition', ['condition', 'action'])
            _validateNodes('/Formalisms/ParallelDEVS/ParallelDEVS/ConfluentTransition', ['condition', 'action'])
            _validateNodes('/Formalisms/ParallelDEVS/ParallelDEVS/channel', ['transfer_function'])
            _validateNodes('/Formalisms/ParallelDEVS/ParallelDEVS/Simulation', ['end_condition'])
            if reqs:
                self._aswCommTools['httpReq']('POST', '/batchEdit', reqs)
        elif self.formalism == self.SCCD:
            pass # TODO implement
        elif self.formalism == self.CBD:
            pass # TODO implement

    def simulate(self):
        self._requestUnhighlight()
        if self.formalism == self.DEVS:
            from exported_to_pypdevs.experiment import termination_condition
            event_name = "simulate"
            params = [{"termination_condition": termination_condition}]
        elif self.formalism == self.SCCD:
            event_name = "continuous"
            params = []
        elif self.formalism == self.CBD:
            from exported_to_cbd.model import numTimeSteps, deltaT
            event_name = "continuous"
            params = [{"nr_of_time_steps": numTimeSteps, "delta_t": deltaT}]
        self.controller.addInput(Event(event_name, "request", params))

    def simulatert(self, scale_factor):
        self._requestUnhighlight()
        if self.formalism == self.DEVS:
            from exported_to_pypdevs.experiment import termination_condition
            params = [{"termination_condition": termination_condition, "realtime_scale": scale_factor}]
        elif self.formalism == self.SCCD:
            params = [{"realtime_scale": scale_factor}]
        elif self.formalism == self.CBD:
            from exported_to_cbd.model import numTimeSteps, deltaT
            params = [{"nr_of_time_steps": numTimeSteps, "realtime_scale": scale_factor, "delta_t": deltaT}]
        self.controller.addInput(Event("realtime", "request", params))

    def pause(self):
        self._requestUnhighlight()
        self.controller.addInput(Event("pause", "request", []))
            
    def timestep(self):
        if self.formalism == self.SCCD:
            self._requestUnhighlight()
            self.controller.addInput(Event("time_step", "request", []))

    def bigstep(self):
        self._requestUnhighlight()
        if self.formalism == self.DEVS:
            from exported_to_pypdevs.experiment import termination_condition
            params = [{"termination_condition": termination_condition}]
        elif self.formalism == self.SCCD:
            params = []
        elif self.formalism == self.CBD:
            from exported_to_cbd.model import numTimeSteps, deltaT
            params = [{"nr_of_time_steps": numTimeSteps, "delta_t": deltaT}]
        self.controller.addInput(Event("big_step", "request", params))
            
    def combostep(self):
        if self.formalism == self.SCCD:
            self.controller.addInput(Event("combo_step", "request", []))
    
    def smallstep(self):
        self._requestUnhighlight()
        if self.formalism == self.DEVS:
            from exported_to_pypdevs.experiment import termination_condition
            params = [{"termination_condition": termination_condition}]
        elif self.formalism == self.SCCD:
            params = []
        elif self.formalism == self.CBD:
            from exported_to_cbd.model import numTimeSteps, deltaT
            params = [{"nr_of_time_steps": numTimeSteps, "delta_t": deltaT}]
        self.controller.addInput(Event("small_step", "request", params))

    def backstep(self):
        if self.formalism == self.DEVS:
            self._requestUnhighlight()
            self.controller.addInput(Event("backwards_step", "request", []))

    def reset(self):
        print 'RESETTING'
        if self.formalism == self.DEVS:
            self._requestUnhighlight()
            self.controller.addInput(Event("reset", "request", []))
        elif self.formalism == self.CBD:
            print 'HERE'
            self._requestUnhighlight()     
            import exported_to_cbd.model
            reload(exported_to_cbd.model)
            from exported_to_cbd.model import Root, numTimeSteps
            from exported_to_cbd.CBDMultipleOutput.Source.cbd_sccd import Controller
            Root.setClock(None)
            self.controller = Controller(Root)
            self.reply_port = self.controller.addOutputListener('reply')
            self.controller.start()
            if '/Formalisms/CBDs/CBD_Debug/Breakpoint' in self.type_map_keys:
                for key in self.type_map_keys['/Formalisms/CBDs/CBD_Debug/Breakpoint']:
                    node = m['nodes'][key]
                    self.breakpoints.append(key)
                    block_id = next(iter(self.outgoing[next(iter(self.outgoing[key]))]))
                    block = self._m_atompm['nodes'][block_id]
                    self.controller.addInput(Event("add_breakpoint", "request", [key, block['$loc'][5:], node['enabled']['value'], node['disable_after_trigger']['value']]))

    def showtrace(self):
        if self.formalism == self.DEVS:
            with open('trace.txt') as f:
                self._aswCommTools['httpReq']('PUT','/GET/console', {'text': f.read()})
        elif self.formalism == self.SCCD:
            self.controller.addInput(Event("get_output_trace", "request", []))

    def godevent(self, data):
        if self.formalism == self.DEVS:
            reqs = []
            value = eval(data['value'])
            if data['attribute'] == 'name':
                reqs.append(self._changeAttr(data['model'] + '.' + value, 'current', True))
                for locstate in self._m[data['model'] + '.' + '$states']:
                    if locstate['name']['value'] != value:
                        reqs.append(self._changeAttr(data['model'] + '.' + locstate['name']['value'], 'current', False))
            else:
                for locstate in self._m[data['model'] + '.' + '$states']:
                    if locstate['current']['value'] == True:
                        attr_vals = self._m_atompm['nodes'][self._m[data['model'] + '.$state']['$key']]['attribute_values']['value']
                        attr_val = filter(lambda x: x['name'] == data['attribute'], attr_vals)
                        json_value = None
                        try:
                            json_value = json.dumps(value)
                        except TypeError:
                            json_value = str(value)
                        if len(attr_val) == 0:
                            attr_vals.append({'name': data['attribute'], 'val': json_value})
                        else:
                            attr_val[0]['val'] = json_value
                        reqs.append(self._changeAttr(data['model'] + '.$state', 'attribute_values', attr_vals))
            if reqs:
                self._aswCommTools['httpReq']('POST', '/batchEdit', reqs)
            self.controller.addInput(Event("god_event", "request", [{"model": data['model'], "attribute": data['attribute'], "value": value}]))
        elif self.formalism == self.SCCD:
            attr_value = eval(data['value'])
            attr_name = data['attribute']
            self.controller.addInput(Event("god_event", "request", [attr_name, attr_value]))

    def inject(self):
        if self.formalism == self.DEVS:
            for msg_id in self.type_map_keys['/Formalisms/ParallelDEVS/ParallelDEVS_Runtime/EventInstance']:
                msg = self._m_atompm['nodes'][msg_id]
                if not 'injected' in msg:
                    if msg_id in self.outgoing:
                        conn_id = next(iter(self.outgoing[msg_id]))
                        port_id = next(iter(self.outgoing[conn_id]))
                        conn = self._m_atompm['nodes'][conn_id]
                        self.controller.addInput(Event("inject", "request", [{"time": conn['time']['value'] if conn['time']['value'] != -1 else self._m_atompm['nodes'][self._m['$simulation']['$key']]['simulation_time']['value'], "port": self._m_atompm['nodes'][port_id]['$loc'], "event": eval(msg['event_type']['value'] + '(' + ','.join(map(lambda x: '%s = %s' % (x['name'], x['val']), msg['attribute_values']['value'])) + ')')}]))
                        msg['injected'] = True
                        msg['injection_time'] = conn['time']['value']
    
    def sccdinject(self, event_name, event_port, event_params=[]):
        if self.formalism == self.SCCD:
            self.controller.addInput(Event("add_input", "request", [Event(event_name, event_port, event_params)]))
        
    def onchangelog(self, c):
        if self.formalism == self.DEVS:
            bp_connected = None
            message_connected = None
            for cl in c['changelog']:
                if cl["op"] == 'CHATTR':
                    self._m_atompm['nodes'][cl['id']][cl['attr']]['value'] = cl['new_val']
                if cl["op"] == 'RMNODE' and cl['id'] in self.messages:
                    self.messages.remove(cl['id'])
                if cl["op"] == 'MKNODE':
                    node = json.loads(cl['node'])
                    self._m_atompm['nodes'][str(cl['id'])] = node
                    self.type_map[node['$type']].append(node)
                    self.type_map_keys[node['$type']].append(str(cl['id']))
                if cl["op"] == 'MKNODE' and json.loads(cl['node'])["$type"] == '/Formalisms/ParallelDEVS/ParallelDEVS_Runtime/EventInstance':
                    self.messages.append(str(cl["id"]))
                elif cl["op"] == 'MKNODE' and json.loads(cl['node'])["$type"] == '/Formalisms/ParallelDEVS/ParallelDEVS_Debug/GlobalBreakpoint':
                    node = json.loads(cl['node'])
                    self.breakpoints.append(str(cl["id"]))
                    condition = '\n'.join(map(lambda l: '\t%s' % l, node['condition']['value'].split("\n")))
                    bp_f = 'def breakpoint(time, model, transitioned):\n%s' % condition
                    self.controller.addInput(Event("add_breakpoint", "request", [str(cl['id']), bp_f, node['enabled']['value'], node['disable_after_trigger']['value']]))
                elif cl["op"] == 'MKNODE' and json.loads(cl['node'])["$type"] == '/Formalisms/ParallelDEVS/ParallelDEVS_Debug/Breakpoint':
                    self.breakpoints.append(str(cl["id"]))
                elif cl["op"] == 'CHATTR' and cl["id"] in self.breakpoints:
                    if cl["attr"] == "condition":
                        self.controller.addInput(Event("del_breakpoint", "request", [cl['id']]))
                        node = self._m_atompm['nodes'][cl["id"]]
                        node['condition']['value'] = cl['new_val']
                        condition = '\n'.join(map(lambda l: '\t%s' % l, cl['new_val'].split("\n")))
                        if node['$type'] == '/Formalisms/ParallelDEVS/ParallelDEVS_Debug/GlobalBreakpoint':
                            bp_f = 'def breakpoint(time, model, transitioned):\n\ttime = time[0]\n%s' % condition
                        else:
                            state_id = next(iter(self.outgoing[next(iter(self.outgoing[cl["id"]]))]))
                            state = self._m_atompm['nodes'][state_id]
                            bp_f = 'def breakpoint(time, model, transitioned):\n\ttime = time[0]\n\tif len(filter(lambda x: x.getModelFullName() == "%s" and x.state.name == "%s", transitioned)) == 0:\n\t\treturn False\n%s' % (state['$loc'][:state['$loc'].rfind(".")], state['name']['value'], condition)
                        self.controller.addInput(Event("add_breakpoint", "request", [cl['id'], bp_f, node['enabled']['value'], node['disable_after_trigger']['value']]))
                    elif cl["attr"] == "enabled":
                        node = self._m_atompm['nodes'][cl["id"]]
                        node['enabled']['value'] = cl['new_val']
                        self.controller.addInput(Event("toggle_breakpoint", "request", [cl["id"], node['enabled']['value']]))
                    elif cl["attr"] == "disable_after_trigger":
                        self.controller.addInput(Event("del_breakpoint", "request", [cl['id']]))
                        node = self._m_atompm['nodes'][cl["id"]]
                        node['disable_after_trigger']['value'] = cl['new_val']
                        condition = '\n'.join(map(lambda l: '\t%s' % l, node['condition']['value'].split('\n')))
                        if node['$type'] == '/Formalisms/ParallelDEVS/ParallelDEVS_Debug/GlobalBreakpoint':
                            bp_f = 'def breakpoint(time, model, transitioned):\n\ttime = time[0]\n%s' % condition
                        else:
                            state_id = next(iter(self.outgoing[next(iter(self.outgoing[cl["id"]]))]))
                            state = self._m_atompm['nodes'][state_id]
                            bp_f = 'def breakpoint(time, model, transitioned):\n\ttime = time[0]\n\tif len(filter(lambda x: x.getModelFullName() == "%s" and x.state.name == "%s", transitioned)) == 0:\n\t\treturn False\n%s' % (state['$loc'][:state['$loc'].rfind(".")], state['name']['value'], condition)
                        self.controller.addInput(Event("add_breakpoint", "request", [cl['id'], bp_f, node['enabled']['value'], node['disable_after_trigger']['value']]))
                elif cl["op"] == 'RMNODE' and cl["id"] in self.breakpoints:
                    self.breakpoints.remove(cl["id"])
                    self.controller.addInput(Event("del_breakpoint", "request", [cl["id"]]))
                elif cl["op"] == 'MKEDGE':
                    self._m_atompm['edges'].append({'src': cl['id1'], 'dest': cl['id2']})
                    self.__calc_in_out_rel('/Formalisms/ParallelDEVS/ParallelDEVS_Debug/target')
                    self.__calc_in_out_rel('/Formalisms/ParallelDEVS/ParallelDEVS_Runtime/injectLocation')
                    if cl['id1'] in self.breakpoints:
                        bp_connected = cl['id1']
                    elif cl['id1'] in self.messages:
                        message_connected = cl['id1']
            if bp_connected:
                state_id = next(iter(self.outgoing[next(iter(self.outgoing[bp_connected]))]))
                node = self._m_atompm['nodes'][bp_connected]
                condition = '\n'.join(map(lambda l: '\t%s' % l, node['condition']['value'].split("\n"))) if len(node['condition']['value']) > 0 else 'return True'
                bp_f = 'def breakpoint(time, model, transitioned):\n\ttime = time[0]\n\tif not "%s" in transitioned:\n\t\treturn False\n%s' % (self._m_atompm['nodes'][state_id]['$loc'], condition)
                self.controller.addInput(Event("add_breakpoint", "request", [bp_connected, bp_f, node['enabled']['value'], node['disable_after_trigger']['value']]))
        if self.formalism == self.SCCD:
            bp_connected = None            
            for cl in c['changelog']:
                if cl["op"] == 'CHATTR':
                    self._m_atompm['nodes'][cl['id']][cl['attr']]['value'] = cl['new_val']
                    self.messages.remove(cl['id'])
                if cl["op"] == 'MKNODE':
                    node = json.loads(cl['node'])
                    self._m_atompm['nodes'][str(cl['id'])] = node
                    self.type_map[node['$type']].append(node)
                    self.type_map_keys[node['$type']].append(str(cl['id']))
                if cl["op"] == 'MKNODE' and json.loads(cl['node'])["$type"] == '/Formalisms/SCCD/SCCD_Debug/GlobalBreakpoint':
                    node = json.loads(cl['node'])
                    self.breakpoints.append(str(cl["id"]))
                    condition = '\n'.join(map(lambda l: '\t%s' % l, node['condition']['value'].split("\n")))
                    bp_f = 'def breakpoint(time, state):\n%s' % condition
                    self.controller.addInput(Event("add_breakpoint", "request", [str(cl['id']), bp_f, node['enabled']['value'], node['disable_after_trigger']['value']]))
                elif cl["op"] == 'MKNODE' and json.loads(cl['node'])["$type"] == '/Formalisms/SCCD/SCCD_Debug/Breakpoint':
                    self.breakpoints.append(str(cl["id"]))
                elif cl["op"] == 'MKEDGE':
                    self._m_atompm['edges'].append({'src': cl['id1'], 'dest': cl['id2']})
                    self.__calc_in_out_rel('/Formalisms/SCCD/SCCD_Debug/target')
                    if cl['id1'] in self.breakpoints:
                        bp_connected = cl['id1']
                elif cl["op"] == 'CHATTR' and cl["id"] in self.breakpoints:
                    if cl["attr"] == "condition":
                        self.controller.addInput(Event("del_breakpoint", "request", [cl['id']]))
                        node = self._m_atompm['nodes'][cl["id"]]
                        node['condition']['value'] = cl['new_val']
                        condition = '\n'.join(map(lambda l: '\t%s' % l, cl['new_val'].split("\n")))
                        if node['$type'] == '/Formalisms/SCCD/SCCD_Debug/GlobalBreakpoint':
                            bp_f = 'def breakpoint(time, state):\n%s' % condition
                        else:
                            state_id = next(iter(self.outgoing[next(iter(self.outgoing[cl["id"]]))]))
                            state = self._m_atompm['nodes'][state_id]
                            bp_f = 'def breakpoint(time, state):\n\tstates=set([])\n\tfor v in state.itervalues():\n\t\tstates.update(v)\n\tif not "%s" in states:\n\t\treturn False\n%s' % (state['$loc'], condition)
                        self.controller.addInput(Event("add_breakpoint", "request", [cl['id'], bp_f, node['enabled']['value'], node['disable_after_trigger']['value']]))
                    elif cl["attr"] == "enabled":
                        node = self._m_atompm['nodes'][cl["id"]]
                        node['enabled']['value'] = cl['new_val']
                        self.controller.addInput(Event("toggle_breakpoint", "request", [cl["id"], node['enabled']['value']]))
                    elif cl["attr"] == "disable_after_trigger":
                        self.controller.addInput(Event("del_breakpoint", "request", [cl['id']]))
                        node = self._m_atompm['nodes'][cl["id"]]
                        node['disable_after_trigger']['value'] = cl['new_val']
                        condition = '\n'.join(map(lambda l: '\t%s' % l, node['condition']['value'].split('\n')))
                        if node['$type'] == '/Formalisms/SCCD/SCCD_Debug/GlobalBreakpoint':
                            bp_f = 'def breakpoint(time, state):\n%s' % condition
                        else:
                            state_id = next(iter(self.outgoing[next(iter(self.outgoing[cl["id"]]))]))
                            state = self._m_atompm['nodes'][state_id]
                            bp_f = 'def breakpoint(time, state):\n\tstates=set([])\n\tfor v in state.itervalues():\n\t\tstates.update(v)\n\tif not "%s" in states:\n\t\treturn False\n%s' % (state['$loc'], condition)
                        self.controller.addInput(Event("add_breakpoint", "request", [cl['id'], bp_f, node['enabled']['value'], node['disable_after_trigger']['value']]))
                elif cl["op"] == 'RMNODE' and cl["id"] in self.breakpoints:
                    self.breakpoints.remove(cl["id"])
                    self.controller.addInput(Event("del_breakpoint", "request", [cl["id"]]))
            if bp_connected:                
                state_id = next(iter(self.outgoing[next(iter(self.outgoing[bp_connected]))]))
                node = self._m_atompm['nodes'][bp_connected]
                condition = '\n'.join(map(lambda l: '\t%s' % l, node['condition']['value'].split("\n"))) if len(node['condition']['value']) > 0 else '\treturn True'
                bp_f = 'def breakpoint(time, state):\n\tstates=set([])\n\tfor v in state.itervalues():\n\t\tstates.update(v)\n\tif not "%s" in states:\n\t\treturn False\n%s' % (self._m_atompm['nodes'][state_id]['$loc'], condition)
                self.controller.addInput(Event("add_breakpoint", "request", [bp_connected, bp_f, node['enabled']['value'], node['disable_after_trigger']['value']]))
        if self.formalism == self.CBD:
            bp_connected = None            
            for cl in c['changelog']:
                if cl["op"] == 'CHATTR':
                    self._m_atompm['nodes'][cl['id']][cl['attr']]['value'] = cl['new_val']
                if cl["op"] == 'MKNODE':
                    node = json.loads(cl['node'])
                    self._m_atompm['nodes'][str(cl['id'])] = node
                    self.type_map[node['$type']].append(node)
                    self.type_map_keys[node['$type']].append(str(cl['id']))
                if cl["op"] == 'MKNODE' and json.loads(cl['node'])["$type"] == '/Formalisms/CBDs/CBD_Debug/Breakpoint':
                    self.breakpoints.append(str(cl["id"]))
                elif cl["op"] == 'MKEDGE':
                    self._m_atompm['edges'].append({'src': cl['id1'], 'dest': cl['id2']})
                    self.__calc_in_out_rel('/Formalisms/CBDs/CBD_Debug/target')
                    if cl['id1'] in self.breakpoints:
                        bp_connected = cl['id1']
                elif cl["op"] == 'CHATTR' and cl["id"] in self.breakpoints:
                    if cl["attr"] == "enabled":
                        node = self._m_atompm['nodes'][cl["id"]]
                        node['enabled']['value'] = cl['new_val']
                        self.controller.addInput(Event("toggle_breakpoint", "request", [cl["id"], node['enabled']['value']]))
                    elif cl["attr"] == "disable_after_trigger":
                        self.controller.addInput(Event("del_breakpoint", "request", [cl['id']]))                        
                        node = m['nodes'][cl["id"]]
                        node['disable_after_trigger']['value'] = cl['new_val']
                        block_id = next(iter(self.outgoing[next(iter(self.outgoing[cl["id"]]))]))
                        block = self._m_atompm['nodes'][block_id]
                        self.controller.addInput(Event("add_breakpoint", "request", [cl["id"], block['$loc'][5:], node['enabled']['value'], node['disable_after_trigger']['value']]))
                elif cl["op"] == 'RMNODE' and cl["id"] in self.breakpoints:
                    self.breakpoints.remove(cl["id"])
                    self.controller.addInput(Event("del_breakpoint", "request", [cl["id"]]))
            if bp_connected:
                print 'bp_connected!'
                block_id = next(iter(self.outgoing[next(iter(self.outgoing[bp_connected]))]))
                block = self._m_atompm['nodes'][block_id]
                node = self._m_atompm['nodes'][bp_connected]
                self.controller.addInput(Event("add_breakpoint", "request", [bp_connected, block['$loc'][5:], node['enabled']['value'], node['disable_after_trigger']['value']]))

    def _simulate(self):
        stateChange = None
        while True:
            stateChange = self.reply_port.fetch(0)
            if stateChange is None:
                break
            self._handleStateChange(stateChange)
        utils.setTimeout(0.1, self._simulate)

    def _changeAttr(self, location, attr_name, attr_val):
        atompm_id = self._m[location]['$key']
        return {'method': 'PUT',
                'uri': self._m[location]['$type'] + '/' + atompm_id + '.instance',
                'reqData': {'changes': {attr_name: attr_val}}}

    def _changeAttrWithID(self, atompm_id, attr_name, attr_val):
        return {'method': 'PUT',
                'uri': atompm_id + '.instance',
                'reqData': {'changes': {attr_name: attr_val}}}

    def _createNode(self, nodetype):
        return {'method': 'POST',
                'uri': nodetype + '.type',
                'reqData': {'hitchhiker': {}}}

    def _removeNode(self, uri):
        return {'method': 'DELETE',
                'uri': uri}

    def _requestNodeHighlight(self, location, color) :
        atompm_id = self._m[location]['$key']
        return utils.httpReq(
                'PUT',
                '127.0.0.1:8124',
                '/GET/console?wid=' + self._aswCommTools['wid'],
                {'text':'CLIENT_BDAPI :: '+
                    '{"func":"_highlight",'+
                    ' "args":'+
                        '{"asid":"' + atompm_id + '",'+
                        ' "color":"' + color + '"}}'})

    def _requestNodeHighlightWithID(self, atompm_id, color) :
        return utils.httpReq(
                'PUT',
                '127.0.0.1:8124',
                '/GET/console?wid=' + self._aswCommTools['wid'],
                {'text':'CLIENT_BDAPI :: '+
                    '{"func":"_highlight",'+
                    ' "args":'+
                        '{"asid":"' + atompm_id + '",'+
                        ' "color":"' + color + '"}}'})

    def _requestUnhighlight(self) :
        return utils.httpReq(
                'PUT',
                '127.0.0.1:8124',
                '/GET/console?wid=' + self._aswCommTools['wid'],
                {'text':'CLIENT_BDAPI :: '+
                    '{"func":"_highlight"}'})

    def _handleStateChange(self, stateChange):
        print stateChange
        if self.formalism == self.DEVS:
            reqs = []
            logging.error(stateChange)
            parameters = stateChange.getParameters()
            event_name = stateChange.getName()
            if event_name != "inject_ok":
                for message_id in self.messages:
                    msg = self._m_atompm['nodes'][message_id]
                    if not 'injected' in msg or msg['injection_time'] < self._m_atompm['nodes'][self._m['$simulation']['$key']]['simulation_time']['value']:
                        reqs.append(self._removeNode('/Formalisms/ParallelDEVS/ParallelDEVS_Runtime/EventInstance/' + str(message_id) + '.instance'))
            if event_name in set(['all_states', 'new_states']):
                for message_id in self.messages:
                    reqs.append(self._removeNode('/Formalisms/ParallelDEVS/ParallelDEVS_Runtime/EventInstance/' + str(message_id) + '.instance'))            
                curr_time = parameters[0][0]
                reqs.append(self._changeAttr('$simulation', 'simulation_time', curr_time))
                for loc, statetuple in parameters[1].iteritems():
                    if isinstance(statetuple, tuple):
                        new_tn = statetuple[0]
                        reqs.append(self._changeAttr(loc, 'time_next', str(new_tn[0])))
                        state = statetuple[1]
                    else:
                        state = statetuple
                    reqs.append(self._changeAttr(loc + '.' + state.name, 'current', True))
                    attr_values = []
                    for attr_name in filter(lambda x: not x.startswith('__') and x != 'name', dir(state)):
                        try:
                            attr_values.append({'name': attr_name, 'val': json.dumps(getattr(state, attr_name))})
                        except TypeError:
                            attr_values.append({'name': attr_name, 'val': str(getattr(state, attr_name))})
                    reqs.append(self._changeAttr(loc + '.$state', 'attribute_values', attr_values))
                    for locstate in self._m[loc + '.' + '$states']:
                        if locstate['name']['value'] != state.name:
                            reqs.append(self._changeAttr(loc + '.' + locstate['name']['value'], 'current', False))
            elif event_name == "imminents":
                curr_time = parameters[0][0]
                reqs.append(self._changeAttr('$simulation', 'simulation_time', curr_time))
                for loc in parameters[1]:
                    self._requestNodeHighlight(loc, "Blue")
            elif event_name in ("inbags", "outbags"):
                curr_time = parameters[0][0]
                reqs.append(self._changeAttr('$simulation', 'simulation_time', curr_time))
                for loc, bags in parameters[1].iteritems():
                    portinstance = self._m[loc]
                    print bags
                    for m in bags:
                        atompm_id = '$%s$' % len(reqs)
                        reqs.append(self._createNode('/Formalisms/ParallelDEVS/ParallelDEVS_Runtime/EventInstance'))
                        reqs.append(self._changeAttrWithID('/Formalisms/ParallelDEVS/ParallelDEVS_Runtime/EventInstance/' + atompm_id, 'position', portinstance['position']['value']))
                        reqs.append(self._changeAttrWithID('/Formalisms/ParallelDEVS/ParallelDEVS_Runtime/EventInstance/' + atompm_id, 'event_type', m.__class__.__name__))
                        if m.__class__.__name__ == 'str':
                            reqs.append(self._changeAttrWithID('/Formalisms/ParallelDEVS/ParallelDEVS_Runtime/EventInstance/' + atompm_id, 'attribute_values', [{'name': 'object', 'val': str(m)}]))
                        else:
                            reqs.append(self._changeAttrWithID('/Formalisms/ParallelDEVS/ParallelDEVS_Runtime/EventInstance/' + atompm_id, 'attribute_values', [{'name': attr_name, 'val': str(getattr(m, attr_name))} for attr_name in filter(lambda x: not x.startswith('__') and x != 'name', dir(m))]))
            elif event_name == "transitioning":
                curr_time = parameters[0][0]
                reqs.append(self._changeAttr('$simulation', 'simulation_time', curr_time))
                for loc, transtype in parameters[1].iteritems():
                    color = {"INTERNAL": "Blue", "EXTERNAL": "Red", "CONFLUENT": "Purple"}[transtype]
                    self._requestNodeHighlight(loc, color)
            elif event_name == "new_tn":
                curr_time = parameters[0][0]
                reqs.append(self._changeAttr('$simulation', 'simulation_time', curr_time))
                for loc, new_tn in parameters[1].iteritems():
                    reqs.append(self._changeAttr(loc, 'time_next', str(new_tn[0])))
            elif event_name == "breakpoint_triggered":
                self._requestNodeHighlightWithID(parameters[0], "blue")
                if self._m_atompm['nodes'][parameters[0]]['disable_after_trigger']['value']:
                    reqs.append(self._changeAttrWithID('/Formalisms/ParallelDEVS/ParallelDEVS_Debug/GlobalBreakpoint/' + parameters[0], 'enabled', False))
            elif event_name == "god_event_ok":
                print 'GOD EVENT RECEIVED'
            elif event_name == "inject_ok":
                print 'INJECT EVENT RECEIVED'
            elif event_name == 'terminate':
                curr_time = parameters[0][0]
                reqs.append(self._changeAttr('$simulation', 'simulation_time', curr_time))
            self._aswCommTools['httpReq']('POST', '/batchEdit', reqs)
        elif self.formalism == self.SCCD:
            reqs = []
            parameters = stateChange.getParameters()
            event_name = stateChange.getName()
            if event_name == "current_state":
                curr_time = parameters[0][0]
                reqs.append(self._changeAttr('$simulation', 'simulation_time', curr_time))
                current_state = parameters[0][1]
                all_states = []
                for state, statelist in current_state.iteritems():
                    all_states.extend(statelist)
                for state_ in self._m:
                    if self._m[state_]['$type'] == "/Formalisms/SCCD/SCCD/BasicState":
                        if state_ in all_states:
                            reqs.append(self._changeAttr(self._m[state_]['$loc'], 'isCurrent', True))
                        else:
                            reqs.append(self._changeAttr(self._m[state_]['$loc'], 'isCurrent', False))
                state_variables = parameters[0][3]
                old_attributes = self._m_atompm['nodes'][self._m['$clz']['$key']]['attributes']['value']
                old_attributes_by_name = {}
                for o_attr in old_attributes:
                    old_attributes_by_name[o_attr['name']] = o_attr['type']
                reqs.append(self._changeAttrWithID('/Formalisms/SCCD/SCCD/Class/' + self._m['$clz']['$key'], 'attributes', [{'name': name, 'type': old_attributes_by_name[name], 'default': val} for name, val in state_variables.iteritems()]))
            elif event_name == "breakpoint_triggered":
                self._requestNodeHighlightWithID(parameters[0], "blue")
                if self._m_atompm['nodes'][parameters[0]]['disable_after_trigger']['value']:
                    reqs.append(self._changeAttrWithID(self._m_atompm['nodes'][parameters[0]]['$type'] + '/' + parameters[0], 'enabled', False))
            elif event_name == "output_trace":
                self._aswCommTools['httpReq']('PUT','/GET/console', {'text': str(parameters[0])})
            elif event_name == "due_events":
                self._aswCommTools['httpReq']('PUT','/GET/console', {'text': 'DUE EVENTS: %s' % str(parameters[0])})
            elif event_name == "god_event_ok":
                state_variables = parameters[0]
                old_attributes = self._m_atompm['nodes'][self._m['$clz']['$key']]['attributes']['value']
                old_attributes_by_name = {}
                for o_attr in old_attributes:
                    old_attributes_by_name[o_attr['name']] = o_attr['type']
                reqs.append(self._changeAttrWithID('/Formalisms/SCCD/SCCD/Class/' + self._m['$clz']['$key'], 'attributes', [{'name': name, 'type': old_attributes_by_name[name], 'default': val} for name, val in state_variables.iteritems()]))
            self._aswCommTools['httpReq']('POST', '/batchEdit', reqs)
        elif self.formalism == self.CBD:
            reqs = []
            parameters = stateChange.getParameters()
            event_name = stateChange.getName()
            if event_name == "current_state":
                numTimeSteps = parameters[0]
                simulatedTime = parameters[1]
                for b_name, value in parameters[2].iteritems():
                    b_lock = 'Root.' + b_name
                    if b_lock.rfind('.') and b_lock[:b_lock.rfind('.')] in self.intder and not b_lock.endswith('OUT1'):
                        continue
                    elif b_lock.rfind('.') and b_lock[:b_lock.rfind('.')] and b_lock.endswith('OUT1'):
                        atompm_block = self._m[b_lock[:b_lock.rfind('.')]]
                    else:
                        atompm_block = self._m[b_lock]
                    for relkey in self.outgoing[atompm_block['$key']]:
                        rel = self._m_atompm['nodes'][relkey]
                        reqs.append(self._changeAttrWithID(rel['$type'] + '/' + rel['$key'], 'value', value[-1].value if value else 0.0))
                    if atompm_block['name']['value'] in self.to_graph:
                        for item in self.to_graph[atompm_block['name']['value']]:
                            plotid, xname = item
                            if numTimeSteps > 0:
                                import time
                                curr_time = time.time()
                                with open("%s.gnuplotcmds" % b_name, "w") as f:
                                    f.write('set terminal svg enhanced background rgb "white"\n')
                                    f.write('set output "%s_%s.svg"\n' % (b_name, curr_time))
                                    f.write('set xlabel "%s"\n' % xname)
                                    f.write('set ylabel "%s"\n' % b_name)
                                    f.write('plot "-" using 1:2 with points lt rgb "red" title "%s---%s"\n' % (xname, b_name))
                                    if xname == '$time':
                                        for signalvalue in value:
                                            f.write('\t\t%s %s\n' % (signalvalue.time, signalvalue.value))
                                    else:
                                        xloc = 'Root.%s' % xname
                                        xblock = self._m[xloc]
                                        if xblock['$type'] in ('/Formalisms/CBDs/CBD_Runtime/DerivatorBlock', '/Formalisms/CBDs/CBD_Runtime/IntegratorBlock'):
                                            signalname = xname + '.OUT1'
                                        else:
                                            signalname = xname
                                        print signalname
                                        for b_name2, value2 in parameters[2].iteritems():                                    
                                            if b_name2 == signalname:
                                                for i in range(len(value)):
                                                    f.write('\t\t%s %s\n' % (value2[i].value, value[i].value))
                                call(["gnuplot", "%s.gnuplotcmds" % b_name])
                                os.rename('%s_%s.svg' % (b_name, curr_time), 'users/devsdebugger/Formalisms/CBDs/plots/%s_%s.svg' % (b_name, curr_time))
                                reqs.append(self._changeAttrWithID('/Formalisms/CBDs/CBD_Runtime/Plot/' + plotid, 'filename', '%s_%s.svg' % (b_name, curr_time)))
                            else:
                                reqs.append(self._changeAttrWithID('/Formalisms/CBDs/CBD_Runtime/Plot/' + plotid, 'filename', 'defaultImage.png'))
                reqs.append(self._changeAttr('$simulation', 'CurrTimeStep', numTimeSteps))
                reqs.append(self._changeAttr('$simulation', 'SimulatedTime', simulatedTime))                    
            elif event_name == "breakpoint_triggered":
                self._requestNodeHighlightWithID(parameters[0], "blue")
                if self._m_atompm['nodes'][parameters[0]]['disable_after_trigger']['value']:
                    reqs.append(self._changeAttrWithID(self._m_atompm['nodes'][parameters[0]]['$type'] + '/' + parameters[0], 'enabled', False))
            elif event_name == "non_linear_found":
                for b_name in parameters[0]:
                    self._requestNodeHighlight('Root.' + b_name, "red")
            self._aswCommTools['httpReq']('POST', '/batchEdit', reqs)