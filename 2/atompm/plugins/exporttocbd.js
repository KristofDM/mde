{
	
	'interfaces': [{'method':'POST', 'url=':'/exportmtocbd'}],
	'csworker':
		function(resp, method, uri, reqData, wcontext) {
			var actions = [__wHttpReq('GET', '/current.model?wid=' + wcontext.__aswid)];
			_do.chain(actions) (
				function(asdata) {
					var writeActions = [_fspp.mkdirs('./exported_to_cbd/')];
					_do.chain(writeActions) (
						function() {
                            if (!String.format) {
                              String.format = function(format) {
                                var args = Array.prototype.slice.call(arguments, 1);
                                return format.replace(/{(\d+)}/g, function(match, number) { 
                                  return typeof args[number] != 'undefined'
                                    ? args[number] 
                                    : match
                                  ;
                                });
                              };
                            }
							var file_contents = '',
								as = _utils.jsonp(asdata['data']),
								type_map = {},
								type_map_keys = {},
								incoming = {},
								outgoing = {};
							
							file_contents += 'from CBDMultipleOutput.Source.CBD import *\n'
							file_contents += 'Root = CBD("Root", output_ports=[], input_ports=[])\n'
							
							for (var key in as.nodes) {
								var node = as.nodes[key];
								if (!(node['$type'] in type_map)) {
									type_map[node['$type']] = [];
									type_map_keys[node['$type']] = [];
								}
								type_map[node['$type']].push(node);
								type_map_keys[node['$type']].push(key);
								node['$key'] = key;
							}
							
							function safe_add(l, el) {
								if (l.indexOf(el) == -1) {
									l.push(el);
								}
							}
							
							function calc_in_out_rel(rel_type) {
								for (var idx in type_map_keys[rel_type]) {
									key = type_map_keys[rel_type][idx];
									incoming[key] = [];
									outgoing[key] = [];
									for (var e_key in as.edges) {
										var e = as.edges[e_key];
										if (e['dest'] == key) {
											safe_add(incoming[key], e['src'])
											if (!(e['src'] in outgoing)) {
												outgoing[e['src']] = [];
											}
											safe_add(outgoing[e['src']], key);
										}
										if (e['src'] == key) {
											safe_add(outgoing[key], e['dest'])
											if (!(e['dest'] in incoming)) {
												incoming[e['dest']] = [];
											}
											safe_add(incoming[e['dest']], key);
										}
									}
								}
							}
							
							calc_in_out_rel('/Formalisms/CBDs/CBD/contents');
							calc_in_out_rel('/Formalisms/CBDs/CBD/rcontents');
							calc_in_out_rel('/Formalisms/CBDs/CBD/cbd_ports');
							calc_in_out_rel('/Formalisms/CBDs/CBD/rports');
							calc_in_out_rel('/Formalisms/CBDs/CBD/IN');
							calc_in_out_rel('/Formalisms/CBDs/CBD/LeftOperand');
							calc_in_out_rel('/Formalisms/CBDs/CBD/RightOperand');
							calc_in_out_rel('/Formalisms/CBDs/CBD/IN_N');
							calc_in_out_rel('/Formalisms/CBDs/CBD/Delay_IN');
							calc_in_out_rel('/Formalisms/CBDs/CBD/IC');
							calc_in_out_rel('/Formalisms/CBDs/CBD/deltaT_der');
							calc_in_out_rel('/Formalisms/CBDs/CBD/deltaT_int');
                            
                            var cbd_creation_strings = {},
                                cbd_type_to_names = {};
                            
                            for (var cbd_idx in type_map['/Formalisms/CBDs/CBD/CBD']) {
                                var curr_cbd = type_map['/Formalisms/CBDs/CBD/CBD'][cbd_idx],
                                    key = type_map_keys['/Formalisms/CBDs/CBD/CBD'][cbd_idx],
                                    blocks = outgoing[key].filter(function(el) {return as.nodes[el]['$type'] == '/Formalisms/CBDs/CBD/contents'}).map(function(blockid) {return outgoing[blockid][0]}).map(function(bid) {return as.nodes[bid]}),
                                    refblocks = outgoing[key].filter(function(el) {return as.nodes[el]['$type'] == '/Formalisms/CBDs/CBD/rcontents'}).map(function(blockid) {return outgoing[blockid][0]}).map(function(bid) {return as.nodes[bid]}),
                                    block_create_strings = [],
                                    block_connection_strings = [],
                                    cbd_creation_string = "";
                                    
                                for (var b_idx in blocks) {
                                    var b = blocks[b_idx],
                                        b_key = b['$key'],
                                        block_name = '"' + b['name']['value'] + '"',
                                        block_type = b['$type'].substr(b['$type'].lastIndexOf('/') + 1),
                                        ctor_params = {'block_name': block_name};
                                    if (block_type == 'InputPort' || block_type == 'OutputPort') {
                                        continue;
                                    } else if (block_type == 'ConstantBlock') {
                                        ctor_params['value'] = b['value']['value'];
                                    } else if (block_type == 'OrBlock' || block_type == 'AndBlock') {
                                        var nr_of_inputs = incoming[b_key].filter(function(el) {return as.nodes[el]['$type'] == '/Formalisms/CBDs/CBD/IN_N'}).length;
                                        ctor_params['numberOfInputs'] = nr_of_inputs;
                                    }
                                    block_create_strings.push(block_type + '(' + Object.keys(ctor_params).map(function(k) {return k + '=' + ctor_params[k]}).join(', ') + ')');
                                    
                                    // TODO: check what kind of block we are, and what kinds of ports we have to link to: for example, for comparison operators, there's IN1 and IC.
                                    for (var i_idx in incoming[b_key]) {
                                        var in_conn = as.nodes[incoming[b_key][i_idx]],
                                            in_block = as.nodes[incoming[incoming[b_key][i_idx]][0]],
                                            input_port_name = 'None';
                                        if (in_conn['$type'] == '/Formalisms/CBDs/CBD/LeftOperand') {
                                            input_port_name = '"IN1"';
                                        } else if (in_conn['$type'] == '/Formalisms/CBDs/CBD/RightOperand') {
                                            if (block_type == 'LessThanBlock' || block_type == 'EqualsBlock') {
                                                input_port_name = '"IC"';
                                            } else {
                                                input_port_name = '"IN2"';
                                            }
                                        } else if (in_conn['$type'] == '/Formalisms/CBDs/CBD/deltaT_int' || in_conn['$type'] == '/Formalisms/CBDs/CBD/deltaT_der') {
                                            input_port_name = '"delta_t"';
                                        } else if (in_conn['$type'] == '/Formalisms/CBDs/CBD/IC') {
                                            input_port_name = '"IC"';
                                        }
                                        if (in_block['$type'] == '/Formalisms/CBDs/CBD/InputPort') {
                                            var in_block_key = incoming[incoming[b_key][i_idx]][0],
                                                cbd_def = as.nodes[incoming[incoming[in_block_key].filter(function(el) {return as.nodes[el]['$type'] == '/Formalisms/CBDs/CBD/cbd_ports'})[0]][0]];
                                            block_connection_strings.push('{0}.addConnection("' + in_block['name']['value'] + '", ' + block_name + ', input_port_name=' + input_port_name + ')\n');
                                        } else if (in_block['$type'] == '/Formalisms/CBDs/CBD/OutputPort') {
                                            var in_block_key = incoming[incoming[b_key][i_idx]][0],
                                                ref_block = as.nodes[incoming[incoming[in_block_key].filter(function(el) {return as.nodes[el]['$type'] == '/Formalisms/CBDs/CBD/rports'})[0]][0]];
                                            block_connection_strings.push('{0}.addConnection("' + ref_block['name']['value'] + '", ' + block_name + ', output_port_name="' + in_block['name']['value'] + '", input_port_name=' + input_port_name + ')\n');
                                        }
                                    }
                                    
                                    for (var o_idx in outgoing[b_key]) {
                                        var out_conn = as.nodes[outgoing[b_key][o_idx]],
                                            out_block = as.nodes[outgoing[outgoing[b_key][o_idx]][0]];
                                        if (out_block['$type'] == '/Formalisms/CBDs/CBD/InputPort') {
                                            var out_block_key = outgoing[outgoing[b_key][o_idx]][0],
                                                ref_block = as.nodes[incoming[incoming[out_block_key].filter(function(el) {return as.nodes[el]['$type'] == '/Formalisms/CBDs/CBD/rports'})[0]][0]];
                                            block_connection_strings.push('{0}.addConnection(' + block_name + ', "' + ref_block['name']['value'] + '", input_port_name="' + out_block['name']['value'] + '")\n');
                                        } else if (out_block['$type'] == '/Formalisms/CBDs/CBD/OutputPort') {
                                            var out_block_key = outgoing[outgoing[b_key][o_idx]][0],
                                                cbd_def = as.nodes[incoming[incoming[out_block_key].filter(function(el) {return as.nodes[el]['$type'] == '/Formalisms/CBDs/CBD/cbd_ports'})[0]][0]];
                                            block_connection_strings.push('{0}.addConnection(' + block_name + ', "' + out_block['name']['value'] + '")\n');
                                        } else if (out_conn['$type'] == '/Formalisms/CBDs/CBD/IN' || out_conn['$type'] == '/Formalisms/CBDs/CBD/Delay_IN' || out_conn['$type'] == '/Formalisms/CBDs/CBD/LeftOperand') {
                                            block_connection_strings.push('{0}.addConnection(' + block_name + ', "' + out_block['name']['value'] + '", input_port_name="IN1")\n');
                                        } else if (out_conn['$type'] == '/Formalisms/CBDs/CBD/RightOperand') {
                                            if (out_block['$type'] == '/Formalisms/CBDs/CBD/LessThanBlock' || out_block['$type'] == '/Formalisms/CBDs/CBD/EqualsBlock') {
                                                input_port_name = '"IC"';
                                            } else {
                                                input_port_name = '"IN2"';
                                            }
                                            block_connection_strings.push('{0}.addConnection(' + block_name + ', "' + out_block['name']['value'] + '", input_port_name=' + input_port_name + ')\n');
                                        } else if (out_conn['$type'] == '/Formalisms/CBDs/CBD/IN_N') {
                                            block_connection_strings.push('{0}.addConnection(' + block_name + ', "' + out_block['name']['value'] + '")\n');
                                        } else if (out_conn['$type'] == '/Formalisms/CBDs/CBD/IC') {
                                            block_connection_strings.push('{0}.addConnection(' + block_name + ', "' + out_block['name']['value'] + '", input_port_name="IC")\n');
                                        } else if (out_conn['$type'] == '/Formalisms/CBDs/CBD/deltaT_der' || out_conn['$type'] == '/Formalisms/CBDs/CBD/deltaT_int') {
                                            block_connection_strings.push('{0}.addConnection(' + block_name + ', "' + out_block['name']['value'] + '", input_port_name="delta_t")\n');
                                        } else {
                                            console.error(out_conn['$type'] + ' not known!');
                                        }
                                    }
                                }
                                
                                for (var rb_idx in refblocks){
                                    var rb = refblocks[rb_idx],
                                        key = rb['$key'],
                                        ports = outgoing[key].filter(function(el) {return as.nodes[el]['$type'] == '/Formalisms/CBDs/CBD/rports'}).map(function(portid) {return outgoing[portid][0]}).map(function(pid) {return as.nodes[pid]});
                                    file_contents += rb['name']['value'] + ' = CBD("' + rb['name']['value'] + '", output_ports=[' + ports.filter(function(n) {return n['$type'] == '/Formalisms/CBDs/CBD/OutputPort'}).map(function(n) {return '"' + n['name']['value'] + '"'}).join(', ') + '], input_ports=[' + ports.filter(function(n) {return n['$type'] == '/Formalisms/CBDs/CBD/InputPort'}).map(function(n) {return '"' + n['name']['value'] + '"'}).join(', ') + '])\n';
                                    cbd_creation_string += curr_cbd['name']['value'] + '.addBlock(' + rb['name']['value'] + ')\n';
                                    if (!(rb['cbd_type']['value'] in cbd_type_to_names)) {
                                        cbd_type_to_names[rb['cbd_type']['value']] = [];
                                    }
                                    cbd_type_to_names[rb['cbd_type']['value']].push(rb['name']['value']);
                                }
                                
                                for (var bcs_idx in block_create_strings) {
                                    cbd_creation_string += '{0}.addBlock(' + block_create_strings[bcs_idx] + ')\n';
                                }
                                for (var bcs_idx in block_connection_strings) {
                                    cbd_creation_string += block_connection_strings[bcs_idx];
                                }
                                
                                if (curr_cbd['name']['value'] == "Root") {
                                    file_contents += String.format(cbd_creation_string, curr_cbd['name']['value']);
                                } else {
                                    cbd_creation_strings[curr_cbd['name']['value']] = cbd_creation_string;
                                }
                            }
                            
                            // add 'reference' blocks (refer to CBDs created in previous step)
                            for (var cbd_type in cbd_type_to_names) {
                                cbd_creation_string = cbd_creation_strings[cbd_type];
                                for (var refname_idx in cbd_type_to_names[cbd_type]) {
                                    file_contents += String.format(cbd_creation_string, cbd_type_to_names[cbd_type][refname_idx]);
                                }
                            }
                            
                            file_contents += 'Root.flatten()\n';
                            file_contents += 'numTimeSteps = ' + type_map['/Formalisms/CBDs/CBD/SimulationParameters'][0]['NumTimeSteps']['value'] + '\n';
                            file_contents += 'deltaT = ' + type_map['/Formalisms/CBDs/CBD/SimulationParameters'][0]['DeltaT']['value'] + '\n';
                            
							_fs.writeFileSync('./exported_to_cbd/model.py', file_contents);
							
							__postMessage({'statusCode': 200,
										   'respIndex': resp});
						},
						function(writeErr) {__postInternalErrorMsg(resp, writeErr);}
					);
				},
				function(err) {__postInternalErrorMsg(resp, err);}
			)
		},
	'asworker':
		function(resp, method, uri, reqData, wcontext)
		{
			__postMessage(
				{'statusCode': 200,
					 'respIndex': resp});	
		}
}
