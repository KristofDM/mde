import Tkinter as tk
from PIL import Image, ImageTk

def run_railway(railway):
    railway.draw()
    railway.step()
    railway.mainloop()

class Railway(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        self.title("Railway")
        self.canvas = tk.Canvas(self)
        self.canvas.pack(side='top', fill='both', expand='yes')
        self.segments = []
        self.trains = []
    
    def addSegment(self, segment):
        self.segments.append(segment)
    
    def addTrain(self, train):
        self.trains.append(train)
        
    def draw(self):
        self.canvas.delete(tk.ALL)
        for segment in self.segments:
            segment.draw(self.canvas)
        self.after(50, self.draw)
    
    def step(self):
        for t in self.trains:
            t.step()
        self.after(1, self.step)

class Segment:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.next = None
        self.prev = []
        self.trains = []
    
    def setLight(self, light):
        self.light = light
    
    def setNext(self, next_segment):
        self.next = next_segment
        next_segment.addPrevious(self)
    
    def addPrevious(self, prev_segment):
        self.prev.append(prev_segment)
    
    def addTrain(self, train):
        self.trains.append(train)
    
    def removeTrain(self, train):
        self.trains.remove(train)
    
    def draw(self, canvas):
        my_id = canvas.create_image(self.x, self.y, image=self.img)
        canvas.tag_bind(my_id, "<Button>", self._onClick)
        for t in self.trains:
            t.draw(canvas, self.x + 3, self.y)
    
    def step(self):
        if self.light.isGreen():
            self.trains[0].setSegment(self.next)
        
    def _setImage(self, img):
        self.img = img
    
    def _onLeftClick(self):
        pass
    
    def _onClick(self, event):
        if event.num == 1:
            self._onLeftClick()

class Straight(Segment):
    def __init__(self, x, y):
        Segment.__init__(self, x, y)
        self._setImage(ImageTk.PhotoImage(Image.open('straight.png')))
    
    def draw(self, canvas):
        Segment.draw(self, canvas)
        self.light.draw(self.x + 3, self.y + 12, canvas)

class Turnout(Segment):
    def __init__(self, x, y, straight=True):
        Segment.__init__(self, x, y)
        self.straight = straight
        self.next_diverging = None
        self.straight_img = ImageTk.PhotoImage(Image.open('turnout_straight.png'))
        self.diverging_img = ImageTk.PhotoImage(Image.open('turnout_diverging.png'))
        self.__setCorrectImg()
    
    def toggle(self):
        self.straight = not self.straight
    
    def setNextDiverging(self, next_segment_diverging):
        self.next_diverging = next_segment_diverging
        self.next_diverging.addPrevious(self)
    
    def draw(self, canvas):
        self.__setCorrectImg()
        Segment.draw(self, canvas)
        self.light.draw(self.x + 3, self.y + 30, canvas)
    
    def step(self):
        if self.straight:
            Segment.step(self)
        else:
            if self.light.isGreen():
                self.trains[0].setSegment(self.next_diverging)
    
    def _onLeftClick(self):
        self.toggle()
    
    def __setCorrectImg(self):
        self._setImage(self.straight_img if self.straight else self.diverging_img)

class Junction(Segment):
    def __init__(self, x, y, straight=True):
        Segment.__init__(self, x, y)
        self.straight = straight
        self.straight_img = ImageTk.PhotoImage(Image.open('junction_straight.png'))
        self.diverging_img = ImageTk.PhotoImage(Image.open('junction_diverging.png'))
        self.__setCorrectImg()
    
    def toggle(self):
        self.straight = not self.straight
    
    def draw(self, canvas):
        self.__setCorrectImg()
        Segment.draw(self, canvas)
        self.light.draw(self.x + 3, self.y - 25, canvas)
    
    def _onLeftClick(self):
        self.toggle()
    
    def __setCorrectImg(self):
        self._setImage(self.straight_img if self.straight else self.diverging_img)

class Station(Segment):
    def __init__(self, x, y, name):
        Segment.__init__(self, x, y)
        self.name = name
        self.normal_img = ImageTk.PhotoImage(Image.open('station_normal.png'))
        self.end_img = ImageTk.PhotoImage(Image.open('station_end.png'))
        self.start_img = ImageTk.PhotoImage(Image.open('station_start.png'))
    
    def draw(self, canvas):
        self.__setCorrectImg()
        Segment.draw(self, canvas)
        self.light.draw(self.x + 3, self.y + 12, canvas)
    
    def __setCorrectImg(self):
        ''' TODO: implement end and start logic '''
        if self.next and self.prev:
            self._setImage(self.normal_img)
        elif self.next:
            self._setImage(self.start_img)
        else:
            self._setImage(self.end_img)

class Train:
    def __init__(self, name):
        self.img = ImageTk.PhotoImage(Image.open("train.png"))
        self.name = name
        self.segment = None
    
    def draw(self, canvas, x, y):
        canvas.create_image(x, y, image=self.img)
        ''' TODO: draw name '''
    
    def setSegment(self, segment):
        if self.segment:
            self.segment.removeTrain(self)
        self.segment = segment
        self.segment.addTrain(self)
    
    def step(self):
        self.segment.step()

class Light:
    def __init__(self, is_green=False):
        self.green = is_green
        self.red_img = ImageTk.PhotoImage(Image.open("redlight.png"))
        self.green_img = ImageTk.PhotoImage(Image.open("greenlight.png"))
    
    def toggle(self):
        self.green = not self.green
    
    def isGreen(self):
        return self.green
    
    def draw(self, x, y, canvas):
        my_id = canvas.create_image(x, y, image=self.green_img if self.green else self.red_img)
        canvas.tag_bind(my_id, "<Button>", self._onClick)
    
    def _onLeftClick(self):
        self.toggle()
    
    def _onClick(self, event):
        if event.num == 1:
            self._onLeftClick()