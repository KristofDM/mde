from Railway import *

railway = Railway()

brussel_1 = Station(50, 50, "Brussel_1")
light_brussel_1 = Light()
brussel_1.setLight(light_brussel_1)
brussel_2 = Station(50, 86, "Brussel_2")
light_brussel_2 = Light()
brussel_2.setLight(light_brussel_2)

junction_brussel = Junction(100, 65)
light_junction_brussel = Light()
junction_brussel.setLight(light_junction_brussel)

straight1 = Straight(150, 50)
light_straight1 = Light()
straight1.setLight(light_straight1)

turnout1 = Turnout(200, 30)
light_turnout1 = Light()
turnout1.setLight(light_turnout1)

straight2 = Straight(250, 50)
light_straight2 = Light()
straight2.setLight(light_straight2)

straight3 = Straight(250, 14)
light_straight3 = Light()
straight3.setLight(light_straight3)

gent = Station(300, 14, "Gent")
light_gent = Light()
gent.setLight(light_gent)
brugge = Station(300, 50, "Brugge")
light_brugge = Light()
brugge.setLight(light_brugge)

brussel_1.setNext(junction_brussel)
brussel_2.setNext(junction_brussel)
junction_brussel.setNext(straight1)
straight1.setNext(turnout1)
turnout1.setNext(straight2)
turnout1.setNextDiverging(straight3)
straight3.setNext(gent)
straight2.setNext(brugge)

railway.addSegment(brussel_1)
railway.addSegment(brussel_2)
railway.addSegment(junction_brussel)
railway.addSegment(straight1)
railway.addSegment(turnout1)
railway.addSegment(straight2)
railway.addSegment(straight3)
railway.addSegment(gent)
railway.addSegment(brugge)

train1 = Train("A0000")
railway.addTrain(train1)
train1.setSegment(brussel_1)
train2 = Train("A0001")
railway.addTrain(train2)
train2.setSegment(brussel_2)

run_railway(railway)
