QUERY: Make sure there can be an independent scheduling: the trains arrived in their endstation and no segment was entered twice. 

SearchNodes (
             EntireGraph,
			 fn n => (
(length(Mark.exported'Antwerpen76 1 n) < 2) andalso
(length(Mark.exported'Gent78 1 n) < 2) andalso
(length(Mark.exported'visited81 1 n) < 2) andalso
(length(Mark.exported'visited88 1 n) < 2) andalso
(length(Mark.exported'junction95 1 n) < 2) andalso
(length(Mark.exported'visitedJunction96 1 n) < 2) andalso
(length(Mark.exported'visited106 1 n) < 2) andalso
(length(Mark.exported'visited114 1 n) < 2) andalso
(length(Mark.exported'down121 1 n) < 2) andalso
(length(Mark.exported'visitedDown122 1 n) < 2) andalso
(length(Mark.exported'visitUp123 1 n) < 2) andalso
(length(Mark.exported'up124 1 n) < 2) andalso
(length(Mark.exported'endStation135 1 n) < 2) andalso
(length(Mark.exported'visitedEndStation136 1 n) < 2) andalso
(length(Mark.exported'endStation143 1 n) < 2) andalso
(length(Mark.exported'visitedEndStation144 1 n) < 2) andalso
(length(Mark.exported'visitedEndStation136 1 n) > 0) andalso
(length(Mark.exported'visitedEndStation144 1 n) > 0) andalso
),
			  NoLimit,
			  fn n => n,
			  [],
			  op ::)
			  
