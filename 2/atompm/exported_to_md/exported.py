from Railway import *

railway = Railway()

# Stations
Station_0 = Station(296.0,410.0034713745117, "Antwerpen")
Station_39 = Station(839.0,465.0034713745117, "Berchem")

# Straights
Straight_1 = Straight(477.0,410.0034713745117)
Straight_41 = Straight(839.0,410.0034713745117)

# Junctions

# Turnouts
Turnout_37 = Turnout(658.0,410.0034713745117, True)


# Lights
Light_5 = Light(False)
Light_6 = Light(False)
Light_43 = Light(False)
Light_44 = Light(False)
Light_45 = Light(False)


# Trains
Train_54 = Train("K1234")
Train_54.setSegment(Station_0)
railway.addTrain(Train_54)
# TODO set train on segment

# Connect all segments
# -> regular segment
Station_0.setNext(Straight_1)
Straight_1.setNext(Turnout_37)
Turnout_37.setNext(Straight_41)

# -> turnout under
Turnout_37.setNextDiverging(Station_39)

# -> all possible connections to junction-turnout
Turnout_37.setNext(Station_39)

# Add lights to segments
Station_0.setLight(Light_5)
Straight_1.setLight(Light_6)
Turnout_37.setLight(Light_43)
Straight_41.setLight(Light_44)
Station_39.setLight(Light_45)

# Add all segments to the railway
railway.addSegment(Turnout_37)
railway.addSegment(Straight_1)
railway.addSegment(Straight_41)
railway.addSegment(Station_0)
railway.addSegment(Station_39)

# Run railway
run_railway(railway)
