from Railway import *

railway = Railway()

# Stations
Station_0 = Station(296.0,410.0034713745117, "Antwerpen")
Station_39 = Station(839.0,465.0034713745117, "Berchem")
Station_57 = Station(1204.0,354.0, "Gent")
Station_94 = Station(272.0,282.10417137451174, "Brussel")

# Straights
Straight_1 = Straight(477.0,410.0034713745117)
Straight_41 = Straight(839.0,410.0034713745117)
Straight_63 = Straight(652.0,295.17007137451174)
Straight_95 = Straight(453.0,282.10417137451174)

# Junctions
Junction_55 = Junction(1023.0,354.0, False)

# Turnouts
Turnout_37 = Turnout(658.0,410.0034713745117, True)
Turnout_61 = Turnout(842.0,299.0, True)


# Lights
Light_5 = Light(False)
Light_6 = Light(False)
Light_43 = Light(False)
Light_44 = Light(False)
Light_45 = Light(False)
Light_69 = Light(False)
Light_72 = Light(False)
Light_74 = Light(False)
Light_76 = Light(False)
Light_98 = Light(False)
Light_99 = Light(False)


# Trains
Train_91 = Train("M1234")
Train_91.setSegment(Station_94)
railway.addTrain(Train_91)
Train_107 = Train("K1234")
Train_107.setSegment(Station_0)
railway.addTrain(Train_107)

# Connect all segments
# -> regular segment
Station_0.setNext(Straight_1)
Straight_1.setNext(Turnout_37)
Turnout_37.setNext(Straight_41)
Junction_55.setNext(Station_57)
Straight_63.setNext(Turnout_61)
Straight_95.setNext(Straight_63)
Station_94.setNext(Straight_95)

# -> all possible connections to junction-turnout
Turnout_61.setNextDiverging(Junction_55) # TopUnder
Straight_41.setNext(Junction_55) # connectSegmentJUnder
Turnout_37.setNextDiverging(Station_39) # connectSegmentTUnder

# Add lights to segments
Station_0.setLight(Light_5)
Straight_1.setLight(Light_6)
Straight_41.setLight(Light_44)
Station_39.setLight(Light_45)
Straight_63.setLight(Light_69)
Junction_55.setLight(Light_74)
Station_57.setLight(Light_76)
Turnout_61.setLight(Light_72)
Turnout_37.setLight(Light_43)
Straight_95.setLight(Light_98)
Station_94.setLight(Light_99)

# Add all segments to the railway
railway.addSegment(Station_0)
railway.addSegment(Station_39)
railway.addSegment(Station_57)
railway.addSegment(Station_94)
railway.addSegment(Junction_55)
railway.addSegment(Turnout_37)
railway.addSegment(Turnout_61)
railway.addSegment(Straight_1)
railway.addSegment(Straight_41)
railway.addSegment(Straight_63)
railway.addSegment(Straight_95)

# Run railway
run_railway(railway)
