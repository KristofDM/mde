QUERY: Make sure there can be an independent scheduling: the trains arrived in their endstation and no segment was entered twice. 

SearchNodes (
             EntireGraph,
			 fn n => (
(length(Mark.exported'Brussel108 1 n) < 2) andalso
(length(Mark.exported'Antwerpen110 1 n) < 2) andalso
(length(Mark.exported'visited113 1 n) < 2) andalso
(length(Mark.exported'visited120 1 n) < 2) andalso
(length(Mark.exported'visited127 1 n) < 2) andalso
(length(Mark.exported'visited135 1 n) < 2) andalso
(length(Mark.exported'visited143 1 n) < 2) andalso
(length(Mark.exported'endStation150 1 n) < 2) andalso
(length(Mark.exported'visitedEndStation151 1 n) < 2) andalso
(length(Mark.exported'visitUp152 1 n) < 2) andalso
(length(Mark.exported'up153 1 n) < 2) andalso
(length(Mark.exported'T2JVisited164 1 n) < 2) andalso
(length(Mark.exported'T2JMove165 1 n) < 2) andalso
(length(Mark.exported'endStation172 1 n) < 2) andalso
(length(Mark.exported'visitedEndStation173 1 n) < 2) andalso
(length(Mark.exported'visitedEndStation151 1 n) > 0) andalso
(length(Mark.exported'visitedEndStation173 1 n) > 0)
),
			  NoLimit,
			  fn n => n,
			  [],
			  op ::)
			  
