\documentclass[12pt1p]{elsarticle}

\usepackage{float} 

\makeatletter
\def\ps@pprintTitle{%
 \let\@oddhead\@empty
 \let\@evenhead\@empty
 \def\@oddfoot{}%
 \let\@evenfoot\@oddfoot}
\makeatother

\usepackage{lineno,hyperref}
\usepackage{natbib}
\modulolinenumbers[5]

\journal{Journal of \LaTeX\ Templates}

%%%%%%%%%%%%%%%%%%%%%%%
%% Elsevier bibliography styles
%%%%%%%%%%%%%%%%%%%%%%%
%% To change the style, put a % in front of the second line of the current style and
%% remove the % from the second line of the style you would like to use.
%%%%%%%%%%%%%%%%%%%%%%%

%% Numbered
%\bibliographystyle{model1-num-names}

%% Numbered without titles
%\bibliographystyle{model1a-num-names}

%% Harvard
%\bibliographystyle{model2-names.bst}\biboptions{authoryear}

%% Vancouver numbered
%\usepackage{numcompress}\bibliographystyle{model3-num-names}

%% Vancouver name/year
%\usepackage{numcompress}\bibliographystyle{model4-names}\biboptions{authoryear}

%% APA style
%\bibliographystyle{model5-names}\biboptions{authoryear}

%% AMA style
%\usepackage{numcompress}\bibliographystyle{model6-num-names}

%% `Elsevier LaTeX' style
\bibliographystyle{elsarticle-num}
%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\nocite{*}

\begin{frontmatter}

\title{Modelling with Papyrus: Domain-Specific Modelling Languages using UML Profiles}


%% Group authors per affiliation:
\author{Kristof De Middelaer}
\address{University of Antwerp \\ \href{mailto:kristof.de.middelaer@gmail.com}{kristof.de.middelaer@gmail.com}}

\begin{abstract}
Domain specific modelling languages (DSML) are often built from scratch. An alternative way of doing this is by taking the Unified Modelling Language (UML) and tailoring it to the needs of the domain. This can be done by using UML's profiling system. We take a look at how we can use Papyrus (an Eclipse based UML2 modelling tool) to accomplish this goal using a Railway DSML as example.
\end{abstract}

\begin{keyword}
UML2, UML Profile, DSML, Papyrus, Modelling, Eclipse
\end{keyword}

\end{frontmatter}

\section{Introduction}
There are numerous reasons why one would want a (domain specific) modelling language. One of the reasons being that it adds another layer of abstraction so that domain experts can more easily build and argue about models without having to learn how to code. For a long time modelling languages weren't standardized. Numerous modelling languages existed that were used for the same purpose.

The Object Modelling Group (OMG) unified some of these modelling languages and created UML: the Unified Modelling Language. UML made it possible to describe systems in such a way so that everyone who understood UML could understand the model.

Nowadays, a lot of UML tools exist. However, a lot of them are proprietary. This makes the users of these tools dependent on the vendor providing the tools which is always a risk. The vendor could decide to stop developing the tool or drop support. For this reason, open source tools are often preferred by the industry and academia. 

The Eclipse project provides an open source tool for graphical modelling of UML2 models: Papyrus. It implements 100\% of the OMG UML specification and has become the de facto standard implementation of the UML2 meta-model. Papyrus also supports the use of UML profiles which makes it suitable to create domain specific languages.

The following sections talk about UML, Papyrus and how they can be used to create domain specific languages. Section 2 talks about UML in general and UML profiles. Section 3 gives information about what relevant functionality Papyrus has to offer. Sections 4 and 5 talk about how a DSML can be built by creating a Papyrus plugin and section 6 talks about further possibilities such as model simulation. In section 7 we will conclude.[TODO]

\section{UML}	
The Unified Modelling Language (UML) is the successor to the wave of object-oriented analysed and design methods that appeared in the late '80s and early '90s. \cite{book:uml_distilled} Instead of everyone using their own language, a standardized modelling language makes it easier to communicate about models. Models also allows us to work at a higher level of abstraction. A model may do this by hiding or masking details, bringing out the big picture, or by focusing on different aspects of the prototype.

UML is a general-purpose modelling language. It helps you specify, visualize and document models of (software) systems. Using a UML-based tool (such as Papyrus) you can analyze the requirements and design a solution that meets them. This solution can then be represented using UML's diagram types.
\subsection{UML diagram types}
UML defines thirteen types of diagrams which are divided into three categories.
\subsubsection{Structure Diagrams}
These diagrams represent static application structure.

\begin{itemize}
  \item Class Diagram
  \item Object Diagram
  \item Component Diagram
  \item Composite Structure Diagram
  \item Package Diagram
  \item Deployment Diagram
\end{itemize}

\subsubsection{Behavior Diagrams}
These diagrams represent general types of behavior.

\begin{itemize}
  \item Use Case Diagram
  \item Activity Diagram
  \item State Machine Diagram
\end{itemize}

\subsubsection{Interaction Diagrams}
These diagrams represent different aspects of interactions.

\begin{itemize}
  \item Sequence Diagram
  \item Communication Diagram
  \item Timing Diagram
  \item Interaction Overview Diagram
\end{itemize}

\subsection{DSML using UML}
Often times a single modelling language will not be enough to cover all the verious concerns involved in a specific area. A domain specific language (DSML) is needed. Instead of building these DSMLs from scratch, UML could be adopted and tailored to the needs of the domain. 

A custom language (that is not based on UML) has the advantage that it can be defined in a way that is optimally suited to the domain. However, imagine each individual subdomain of a complex system using a different modelling language. Getting these submodels into a consistent, integrated whole that can be verified, tested or simply unambiguously understood poses quite the challenge.

If all of system's submodels are based on UML, existing UML tools and widely available UML expertise can be reused. Note that this does not completely solve the integration issues but mitigate these because of the common UML semantics and syntax.

But how do we create a DSML using UML? This is where \textit{UML profiles} come into play.

\subsubsection{UML profiles}
In UML profiles all domain specific concepts are derived as extensions or refinements of existing UML concepts (which are the UML metaclasses).
These extensions are called stereotypes. They are profile classes that extend UML metaclasses. 
\begin{figure}[H]
    \makebox[\linewidth]{
        \includegraphics[width=0.75\textwidth]{img/stereotype.png}
    }
    \caption{\textit{Train} is a stereotype of the Metaclass \textit{Class}}
\end{figure}
These stereotypes can also include tagged values. Since UML 2.0 a tagged value is represented as an attribute defined on a stereotype. 

Stereotypes can also be used to change the graphical appearance of the extended model element by attaching icons.
\begin{figure}[H]
    \makebox[\linewidth]{
        \includegraphics[width=0.50\textwidth]{img/icon.png}
    }
    \caption{A custom icon that is given given to the stereotype.\cite{website:uml_diagrams_org_profile_diagrams}}
\end{figure}

Constraints (such as OCL constraints) can also be defined. These apply to stereotypes defined in the profile. Using these constraints we can further constrain elements of the UML metamodel. For example, we could define an OCL constraint that says that all instances of Class need to have at least one operation. Constraints that can't be expressed in OCL have to be denoted in natural language which can then be rewritten in some language the used UML tool will understand.

\section{Papyrus}
\subsection{About}
Papyrus is an Eclipse-based tool which uses the Eclipse Graphical Modelling Framework (GMF). It aims at providing an integrated and userconsumable environment for editing any kind of Eclipse Modelling Framework (EMF) model (in particular UML and related modelling languages such as SysML\footnote{A dialect of UML for systems engineering applications: \href{http://sysml.org/}{http://sysml.org/}}). Papyrus offers this integrated and userconsumable environment by providing diagram editors for these models.

\subsection{Functionality}
\subsubsection{UML modelling}
Papyrus allows users to build UML2 models that strictly conform to the OMG's UML specification. If something cannot be represented in Papyrus, it generally means that it is not allowed by the specification.\cite{website:newsletter} These models can be created and edited using the graphical editor.

As mentioned before, there are a lot of functions in support of UML2 modelling. To name a few: a tools palette which allows the creation of UML elements in the diagram, a \textit{properties} window that allows you to edit UML elements and OCL constraints can be specified for each UML element which can then be checked.

\begin{figure}[H]
    \makebox[\linewidth]{
        \includegraphics[width=1.1\textwidth]{img/uml-papyrus.png}
    }
    \caption{UML diagram in Papyrus}
\end{figure}

\subsubsection{DSML}
Papyrus provides extensive support for UML profiles. This allows the user to create profiles for their domain which serves as the domain specific modelling language.

The UML profile can change the concrete syntax: instead of the standard UML graphics, custom icons can either be added to these or completely replace them.
Papyrus also provides powerful tool customization capabilities. This way the tools can be customized to better fit the domain.

In other words: when a profile is applied, the tool may adapt its visual rendering and GUI to fit the specific domain supported by that profile.	
\begin{figure}[H]
    \makebox[\linewidth]{
        \includegraphics[width=1\textwidth]{img/DSLexample.png}
    }
    \caption{Customized tool and icons.}
\end{figure}

Eclipse provides the \textit{Papyrus User Guide Series}. It is a series of documents dedicated to assist the usage of Papyrus. There is a volume focused on using UML profiles within Papyrus: from design and definition to application and usage.\cite{documentation:Papyrus_User_Guide_About_UML_Profiling} This 30 pages long document explains how to define and apply a very basic UML profile.

\subsection{Papyrus plug-ins}
As Papyrus is based on Eclipse, it supports the use of plug-ins. What we want to do is create a new Papyrus plug-in for our DSML. This way distribution is easy by either copying the plug-in files into the correct directory or by using an update site. As the following section will contain some Eclipse plug-in references, we'll first take a look at how Eclipse plug-ins work.

When people hear \textit{Eclipse} they usually think about the Eclipse IDE. However, the Eclipse IDE is based on the Eclipse Rich Client Platform (Eclipse RCP) which can be used as a basis to create other sorts of feature-rich stand-alone applications.

The Eclipse RCP provides the existing user interface and the internal framework which can be extended using plug-ins. The Eclipse IDE for example extends the Eclipse RCP with Java development or C development tools as plug-ins.

Papyrus works the same way: it is also based on the Eclipse RPC and provides plugins for UML modelling and all its capabilities. 

Apart from building an extension for the Eclipse RPC it is also possible to write extensions for Papyrus. This is done by creating a plug-in that uses the concept of \textit{extensions} and \textit{extension points}. Papyrus offers a set of \textit{extension points} in which we can 'plug in' our extensions. Concretely: Papyrus offers extension points such as \textit{org.eclipse.papyrus.uml.extensionpoints.UMLProfile} at which we can register a certain profile which then becomes part of our extended Papyrus program.

Eclipse provides a plug-in development environment which can be used to more easily create plugins instead of creating these with raw files.

\begin{figure}[H]
    \makebox[\linewidth]{
        \includegraphics[width=1\textwidth]{img/plugin.png}
    }
    \caption{Creating an Eclipse plugin}
\end{figure}

\newpage

\section{DSML modelling in Papyrus}
In order to create a DSML in Papyrus the following steps need to be followed. First, a general step that is inherent to creating a DSML: identifying domain concepts and relations. In this report the railway domain has been chosen as an example.

Then we need to create a UML Profile that reflects our DSML. We create stereotypes, a concrete syntax etc. 

After this we add constraints in order to be able to validate models created by the user. 

Of course, the user needs to be able to create the models so we need a new type of diagram. In our railway example this will be the \textit{Railway Diagram}.
\begin{figure}[H]
    \makebox[\linewidth]{
        \includegraphics[width=1\textwidth]{img/UMLDSML.png}
    }
    \caption{Creating a DSML}
\end{figure}

\subsection{Identifying domain concepts and relations}
A first, general step in creating a DSML is identifying domain concepts and relations. This step is not explained here as it is not what this report is about.

For the example in this report the railway domain has been chosen. Note that this is more of a toy example to show the DSML functionality of Papyrus instead of a fully correct railway DSML. 
\subsection{Creating the UML profile}
Creating the UML profile is quite a straightforward process.

You first start by creating a new Papyrus Project (or diagram) where you'll create the profile (\textit{file \textgreater new \textgreater Papyrus Project}). This will open up the new Papyrus Project wizard. Most steps are straightforward: when asked to select a diagram language choose \textit{profile}.
\begin{figure}[H]
    \makebox[\linewidth]{
        \includegraphics[width=1\textwidth]{img/newproject.png}
    }
    \caption{Select the Profile diagram language}
\end{figure}



Complete the wizard and when asked, select the Profile Diagram kind. Now we need to create our stereotypes, give them tagged values and add images to these.

Our stereotypes are extended from metaclasses. A metaclass can be added by selecting \textit{import metaclass} from the palette, clicking in the diagram and then selecting the desired metaclass.

\begin{figure}[H]
    \makebox[\linewidth]{
        \includegraphics[width=0.25\textwidth]{img/metaclass.png}
    }
    \caption{Import metaclass from the palette}
\end{figure}

Once this metaclass has been added we can start creating stereotypes that extend these.
\begin{figure}[H]
    \makebox[\linewidth]{
        \includegraphics[width=1\textwidth]{img/extended.png}
    }
    \caption{Metaclass extension}
\end{figure}

We can add tagged values to our stereotypes by selecting \textit{property} from the palette to our stereotype and clicking in the diagram. In the properties of this tagged value we can choose the type, the initial value and other things.

\begin{figure}[H]
    \makebox[\linewidth]{
        \includegraphics[width=1\textwidth]{img/taggedvalue.png}
    }
    \caption{Editing a tagged value}
\end{figure}

Once we have our stereotypes we can add images to these. In the properties of the stereotype under the \textit{UML tab} we can add an icon. There seem to be numerous ways of adding icons to the stereotypes but the only way I found to be working was by adding the location of an svg file to the stereotype and then creating a CSS file that makes the images visible and hide the usual UML boxes when creating an element. This CSS file can be found at \textit{resources/Railway/custom.css}.

A user can manually apply UML profiles. In a true DSML environment it is preferable to hide as much of UML as possible. Automatic application of a UML profile is possible but there is no very user-friendly way of doing this. This is done programmatically by overriding the initializeModel method in a class that extends from \textit{ModelCreationCommandBase} and adding some code here. This can be found in \textit{src/railway/CreateRailwayModelCommand.java}.

\subsection{Adding constraints}
Model validation is an important aspect of using a DSML. Papyrus provides support for constraints in the form of OCL or Java. I found no real documentation about the Java constraints and chose to write OCL constraints as there was more documentation for this and as I tried to stay away from writing Java code as much as possible.

Adding a constraint to a UML profile can be done by selecting \textit{constraint} from the palette and then clicking in the diagram. In this element the constraint can be written by choosing the constraint's context and adding a specification in the \textit{UML tab} in the constraint block's properties.

\begin{figure}[H]
    \makebox[\linewidth]{
        \includegraphics[width=1\textwidth]{img/constraint.png}
    }
    \caption{Adding a constraint}
\end{figure}

An issue I encountered was when trying to create a constraint using \textit{Train.allInstances()}. The returning set was always of size 0 and I had no clue why. I asked on the Papyrus forums and turns out that it was a bug. A bug report was filed and it has been supposedly been fixed.

When creating a model in the DSML, the constraints can be checked by right clicking on the diagram and letting it verify the model. Manually doing this is not ideal. It would be nice to alert the user and prohibiting a certain action at the time the user is doing it. I found no functionality for doing this in the standard Papyrus install. However, I found a validation plug-in that is supposed to support the live evaluation of constraints and some more handy features.

The validation plug-in contains a UML profile that can be applied to the UML profile being created. It contains a stereotype for constraints which provides these advanced features. After creating the constraints, you would generate a validation plug-in that takes care of the constraints. 

Unfortunately, the plug-in doesn't really work as it is supposed to for OCL. It should work with Java constraints but that would be too impractical. The problem is that the OCL constraints are defined and validated in different OCL environments (LPG vs. Pivot) which renders these unuseable. 

\subsection{Creating a custom diagram}
When a DSML user creates a model they'd need to create a new diagram and add elements in that diagram. It's important that this diagram is of a new type as modelling the domain specific language in a UML diagram is not very desireable. Creating an environment that facilitates the use of the DSML and hides all UML would be nice.

Unfortunately, the online Papyrus documentation concerning this is very limited and does not really specify how this can be done. Papyrus comes bundled with SysML support which accomplishes this. So in theory I just had to open the SysML plug-ins and see how this had been done. 

There were numerous SysML plugins which did a lot of things. I had found what extension points I needed to use in order to add a new diagram category and diagram. After some searching I found the \textit{org.eclipse.papyrus.infra.core.papyrusDiagram} extension point which could be used for this. There was little documentation on doing this in the help section. The things I achieved using this approach was quite limited as it was quite complex, impractical and involved a lot of Java coding. I turned to the Papyrus forums for some help and a Camille Letavernier (a Papyrus developer) told me that that was the old way of doing things. There was a new way of doing this which is being used in the new SysML version called viewpoints.

After installing and inspecting the SysML1.4 plug-ins together with the documentation (Help \textgreater Help Contents \textgreater Papyrus Guide \textgreater User Guide \textgreater Viewpoints in Papyrus) I found out how viewpoints were created.

\subsubsection{Viewpoints}
The Papyrus documentation describes viewpoints as follows:
"Viewpoints in Papyrus enable the specialization and customization of the user experience by constraining what can be seen and interacted with in Papyrus diagrams and tables."

The custom CSS file that enables the stereotypes to show icons, the custom palette and 

For our Railway example I created a Railway viewpoint which creates the Railway diagram that is under the Railway category, adds the custom CSS file that enables the stereotypes to show and the custom palette. Normally, creating the Railway category could just be done in the viewpoint.configuration file but that didn't work. Creating the diagram category using the old \textit{org.eclipse.papyrus.infra.core.papyrusDiagram} did work.

Besides the earlier mentioned custom CSS file (that takes care of the concrete syntax) the palette is a very important part of modelling in Papyrus. Elements are added to the diagram by selecting them from the palette and clicking in the diagram. A custom palette can manually be made which generates an XML file in your workspace folder at \textit{.metadata/.plugins/org.eclipse.papyrus.diagram.common}. This XML file can then be included in the plug-in and used in the viewpoint so that it automatically gets loaded.

After defining the viewpoint, it can be added to the Railway plug-in through the \textit{org.eclipse.papyrus.infra.viewpoints.policy.custom} extension point. You can either choose your viewpoint to be a \textit{contribution} or a \textit{configuration}. The contribution choice will add your new viewpoint to the already existing UML/SysML viewpoints. If the configuration element gets chosen your new viewpoint will be the only one used. For our example the user should only be able to build models in our DSML. Hiding the other options will make for a cleaner, less complicated environment.

\subsubsection{Property views}

In Papyrus, every element in a diagram has a properties view. In this properties view one can edit the element's information. In a DSML context it makes sense to show the tagged values and make them editable. In order to get this functionality in the properties tab we have to create a new properties context file. Papyrus provides a wizard to generate such a properties context file from a UML Profile (File \textgreater New \textgreater Other \textgreater Papyrus/Property view configuration). This properties context can be hot deployed as well as be included in the DSML plug-in directly through the \textit{org.eclipse.papyrus.views.properties.context} extension point.

As we're creating a UML profile, numerous tabs in the properties windows in our stereotypes contain UML information. We generally want to hide this information from the user. This can be done by deselecting the contexts we want to hide in the workspace preferences (Window \textgreater Preferences \textgreater Papyrus \textgreater Property views).

\begin{figure}[H]
    \makebox[\linewidth]{
        \includegraphics[width=1\textwidth]{img/propertyviews.png}
    }
    \caption{Selecting the desired contexts}
\end{figure}

\subsubsection{Diagram assistants}\label{creatingcustomdiagram}
Papyrus UML diagrams provide a feature called \textit{diagram assistants} that offer context-sensitive tools directly in the diagram. These assist the user in two ways: there are pop-up bars that contain a variety of tools for the creation of new model elements. The other way the assistants help is by providing connection handles which can be used to easily create new connections to other elements in the diagram.

The capabilities of these diagram assistants can be extended in order to provide assistence for our profile-based DSL. 

Unfortunately, it seems to be that there is no way to hide the existing UML assistance which makes these assistants rather unuseful.

\section{The complete DSML plugin}
\subsection{Exporting and installing the plug-in}
Once the development of the plug-in is finished, it can be exported. In the plugin.xml file there is a wizard that helps you do this. 

In order to use this plugin you can copy the generated .jar file to the plugins folder of your Papyrus installation and restart Papyrus. Another way of doing this is by creating an update site for your plug-in. If you want to do this, I suggest taking a look at the Eclipse documentation on this. 

\subsection{Using the plug-in}
After starting Papyrus with our plug-in installed we need to make some one time adjustments in order to be able to fully use the plug-in. 

Open Window \textgreater Preferences \textgreater Papyrus

\begin{itemize}
	\item In the \textit{Viewpoints Configuration} section: choose "Deployed through the extension point" as configuration selection.
	\item In the \textit{Property Views} section: deselect the contexts we want to hide as described in section \ref{creatingcustomdiagram}.
\end{itemize}

Now you should be able to create a new Papyrus Railway project, create a Railway diagram and create a model using the palette and property view.

\section{Further possibilities}
This report talks about how Papyrus can be used to create a DSML. Often times, after creating a model one would want to simulate this model. 

As Papyrus is based on Eclipse, QVT could be used to simulate. Moka is also an alternative which is well documented. [TODO; elaborate]

Moka: model animation + simulation
QVTo: model-to-model transformation

\section{Conclusion/comparison/feasability}
Domain specific modelling languages have great potential. Building these, together with a DSML environment should be easy and well documented. 

Papyrus has great potential but at the moment of writing this report it has a few issues. It isn't very user friendly to build a DSML environment, I encountered numerous bugs (most of them reported but not fixed) and documentation was lacking and sometimes outdated. If the process of creating the modelling environment could be streamlined and well documented I'm sure Papyrus could gain a bigger userbase.

One of the big strengths of Papyrus is that you can reuse Papyrus as a DSML environment. In order to do so, one has to write a Papyrus plug-in. 

All of this combined means that using Papyrus for domain specific modelling has quite a steep learning curve.

\newpage

\bibliography{mybibfile}

\end{document}