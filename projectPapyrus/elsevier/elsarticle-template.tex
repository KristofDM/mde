\documentclass[review]{elsarticle}
\makeatletter
\def\ps@pprintTitle{%
 \let\@oddhead\@empty
 \let\@evenhead\@empty
 \def\@oddfoot{}%
 \let\@evenfoot\@oddfoot}
\makeatother

\usepackage{lineno,hyperref}
\usepackage{natbib}
\modulolinenumbers[5]

\journal{Journal of \LaTeX\ Templates}

%%%%%%%%%%%%%%%%%%%%%%%
%% Elsevier bibliography styles
%%%%%%%%%%%%%%%%%%%%%%%
%% To change the style, put a % in front of the second line of the current style and
%% remove the % from the second line of the style you would like to use.
%%%%%%%%%%%%%%%%%%%%%%%

%% Numbered
%\bibliographystyle{model1-num-names}

%% Numbered without titles
%\bibliographystyle{model1a-num-names}

%% Harvard
%\bibliographystyle{model2-names.bst}\biboptions{authoryear}

%% Vancouver numbered
%\usepackage{numcompress}\bibliographystyle{model3-num-names}

%% Vancouver name/year
%\usepackage{numcompress}\bibliographystyle{model4-names}\biboptions{authoryear}

%% APA style
%\bibliographystyle{model5-names}\biboptions{authoryear}

%% AMA style
%\usepackage{numcompress}\bibliographystyle{model6-num-names}

%% `Elsevier LaTeX' style
\bibliographystyle{elsarticle-num}
%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\nocite{*}

\begin{frontmatter}

\title{Modelling with Papyrus: Domain-Specific Modelling Languages using UML Profiles}


%% Group authors per affiliation:
\author{Kristof De Middelaer}
\address{University of Antwerp \\ \href{mailto:kristof.de.middelaer@gmail.com}{kristof.de.middelaer@gmail.com}}

\begin{abstract}
Domain specific modelling languages (DSML) are often built from scratch. An alternative way of doing this is by taking the Unified Modelling Language (UML) and tailoring it to the needs of the domain. This can be done by using UML's profiling system. We take a look at how we can use Papyrus (an Eclipse based UML2 modelling tool) to accomplish this goal in the railway domain.
\end{abstract}

\begin{keyword}
UML2, UML Profile, DSML, Papyrus, Modelling, Eclipse
\end{keyword}

\end{frontmatter}

\section{Introduction}
There are numerous reasons why one would want a (domain specific) modelling language. One of the reasons being that it adds another layer of abstraction so that domain experts can more easily build and argue about models without having to learn how to code. For a long time modelling languages weren't standardized. Numerous modelling languages existed that were used for the same purpose.

The Object Modelling Group (OMG) unified some of these modelling languages and created UML: the Unified Modelling Language. UML made it possible to describe systems in such a way so that everyone who understood UML could understand the model.

Nowadays, a lot of UML tools exist. However, a lot of them are proprietary. This makes the users of these tools dependant on the vendor providing the tools which is always a risk. The vendor could decide to stop developing the tool or give no support. For this reason, open source tools are often preferred by the industry and academia. 

The Eclipse project provides an open source tool for graphical modelling of UML2 models: Papyrus. It implements 100\% of the OMG UML specification and has become the de facto standard implementation of the UML2 meta-model. Papyrus also supports the use of UML profiles which makes it suitable to create domain specific languages.

The following sections talk about UML, Papyrus and how they can be used to create domain specific languages. Section 2 talks about UML in general and UML profiles. Section 3 gives information about what relevant functionality Papyrus has to offer. Section 3 talks about how Papyrus might be used to build a railway DSL.

\section{UML}	
The Unified Modelling Language (UML) is the successor to the wave of object-oriented analysed and design methods that appeared in the late '80s and early '90s. \cite{book:uml_distilled} Instead of everyone using their own language, a standardized modelling language makes it easier to communicate about models. Models also allows us to work at a higher level of abstraction. A model may do this by hiding or masking details, bringing out the big picture, or by focusing on different aspects of the prototype.

UML is a general-purpose modelling language. It helps you specify, visualize and document models of (software) systems. Using a UML-based tool (such as Papyrus) you can analyze the requirements and design a solution that meets them. This solution can then be represented using UML's diagram types.
\subsection{UML diagram types}
UML defines thirteen types of diagrams which are divided into three categories.
\subsubsection{Structure Diagrams}
These diagrams represent static application structure.

\begin{itemize}
  \item Class Diagram
  \item Object Diagram
  \item Component Diagram
  \item Composite Structure Diagram
  \item Package Diagram
  \item Deployment Diagram
\end{itemize}

\subsubsection{Behavior Diagrams}
These diagrams represent general types of behavior.

\begin{itemize}
  \item Use Case Diagram
  \item Activity Diagram
  \item State Machine Diagram
\end{itemize}

\subsubsection{Interaction Diagrams}
These diagrams represent different aspects of interactions.

\begin{itemize}
  \item Sequence Diagram
  \item Communication Diagram
  \item Timing Diagram
  \item Interaction Overview Diagram
\end{itemize}

\subsection{DSML using UML}
Often times a single modelling language will not be enough to cover all the verious concerns involved in a specific area. A domain specific language (DSML) is needed. Instead of building these DSMLs from scratch, UML should be adopted and tailored to the needs of the domain. 

A custom language (that is not based on UML) has the advantage that it can be defined in a way that is optimally suited to the domain. However, imagine each individual subdomain of a complex system using a different modelling language. Getting these submodels into a consistent, integrated whole that can be verified, tested or simply unambiguously understood poses quite the challenge.

If all of system's submodels are based on UML, existing UML tools and widely available UML expertise can be reused. Note that this does not completely solve the integration issues but mitigate these because of the common UML semantics and syntax.

But how do we create a DSML using UML? This is where \textit{UML profiles} come into play.

\subsubsection{UML profiles}
In UML profiles all domain specific concepts are derived as extensions or refinements of existing UML concepts (which are the UML metaclasses).
These extensions are called stereotypes. They are profile classes that extend UML metaclasses. 
\begin{figure}[h]
    \makebox[\linewidth]{
        \includegraphics[width=0.75\textwidth]{img/stereotype.png}
    }
    \caption{\textit{Computer} is a stereotype of the Metaclass \textit{Device}.\cite{website:uml_diagrams_org_profile_diagrams}}
\end{figure}
These stereotypes can also include tagged values. Since UML 2.0 a tagged value is represented as an attribute defined on a stereotype. 

Stereotypes can also be used to change the graphical appearance of the extended model element by attaching icons.
\begin{figure}[h]
    \makebox[\linewidth]{
        \includegraphics[width=0.75\textwidth]{img/icon.png}
    }
    \caption{A custom icon that is given given to the stereotype.\cite{website:uml_diagrams_org_profile_diagrams}}
\end{figure}

Constraints (such as OCL constraints) can also be defined. These apply to stereotypes defined in the profile. Using these constraints we can further constrain elements of the UML metamodel. For example, we could define an OCL constraint that says that all instances of Class need to have at least one operation. Constraints that can't be expressed in OCL have to be denoted in natural language which can then be rewritten in some language the used UML tool will understand.

\section{Papyrus}
\subsection{About}
Papyrus is an Eclipse-based tool which uses the Eclipse Graphical Modelling Framework (GMF). It aims at providing an integrated and userconsumable environment for editing any kind of Eclipse Modelling Framework (EMF) model (in particular UML and related modelling languages such as SysML\footnote{A dialect of UML for systems engineering applications: \href{http://sysml.org/}{http://sysml.org/}}). Papyrus offers this integrated and userconsumable environment by providing diagram editors for these models.

\subsection{Functionality}
\subsubsection{UML modelling}
Papyrus allows users to build UML2 models that strictly conform to the OMG's UML specification. If something cannot be represented in Papyrus, it generally means that it is not allowed by the specification.\cite{website:newsletter} These models can be created and edited using the graphical editor.

As mentioned before, there are a lot of functions in support of UML2 modelling. To name a few: a tools palette which allows the creation of UML elements in the diagram, a \textit{properties} window that allows you to edit UML elements and OCL constraints can be specified for each UML element which can then be checked.

\begin{figure}[h]
    \makebox[\linewidth]{
        \includegraphics[width=1.1\textwidth]{img/uml-papyrus.png}
    }
    \caption{UML diagram in Papyrus}
\end{figure}

\subsubsection{DSML}
Papyrus provides extensive support for UML profiles. This allows the user to create profiles for their domain which serves as the domain specific modelling language. 

The UML profile can change the concrete syntax: instead of the standard UML graphics, custom icons can either be added to these or completely replace them.
Papyrus also provides powerful tool customization capabilities. This way the tools can be customized to better fit the domain.

In other words: when a profile is applied, the tool may adapt its visual rendering and GUI to fit the specific domain supported by that profile.	
\begin{figure}[h]
    \makebox[\linewidth]{
        \includegraphics[width=1\textwidth]{img/DSLexample.png}
    }
    \caption{Customized tool and icons.\cite{website:newsletter}}
\end{figure}

Eclipse provides the \textit{Papyrus User Guide Series}. It is a series of documents dedicated to assist the usage of Papyrus. There is a volume focused on using UML profiles within Papyrus: from design and definition to application and usage.\cite{documentation:Papyrus_User_Guide_About_UML_Profiling}

\section{Conclusion: building a railway DSML in Papyrus}
In order to build a functional railway DSML in Papyrus a railway UML profile would need to be made. Stereotypes have to be made and have suitable icons assigned to them. Correct assocations and requirements would have to be enforced and a suitable tool would need to be created in order to efficiently use the railway DSML.

\newpage

\bibliography{mybibfile}

\end{document}