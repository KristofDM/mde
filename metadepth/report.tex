\documentclass[11pt]{article}


\usepackage{fancyhdr}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{enumerate}
\usepackage[greek,english]{babel}
\usepackage{framed}
\usepackage{listings}

\usepackage{tikz}
\usepackage{pgfplots}
\usetikzlibrary{automata,positioning}
\usepackage[applemac]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{arydshln}
\usepackage{amsmath}

\pagestyle{fancy}

\setlength{\parindent}{0cm}

\usepackage{titlesec}
%\newcommand{\sectionbreak}{\clearpage}
\newcommand{\comment}[1]{}

\usepackage{listings}
\usepackage{color}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=Java,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}


\title{Meta-modelling with metaDepth}
\author{Mathias Beke}
\date{\today}


\begin{document}


\lhead{Model Driven Engineering}
\rhead{Mathias Beke - Kristof De Middelaer}

	\begin{center}

	   \vspace*{0.5 cm}

	   \Large\textbf{Meta-modelling with metaDepth}\\

	   \vspace*{0.5 cm}

	   \Large{Mathias Beke - Kristof De Middelaer}\\
	   \emph{20120536 - 20121196}


	\end{center}


\vspace*{1.5 cm}

\tableofcontents


\pagebreak


\section{Abstract Syntax}

\subsection{Railway model}

\emph{The railway model can be found in railway.mdepth}\\

We first started working on the abstract syntax. More specific on the \emph{Railway} model.
The two simple \emph{Train} and \emph{Light} nodes, followed by an abstract \emph{Segment} node, which is extended by the \emph{Straight}, \emph{Turnout}, \emph{Junction} and \emph{Station} nodes.\\

Once we had the model implemented we could start working on the constraints for this model.

After solving some basic syntactical errors we encountered our first error we didn't quickly understand:

\begin{lstlisting}
> :: EOL parse error

:: 2 EOL error(s):
:: Line: 1, Column: 52, Reason: mismatched input: '='
:: Line: 1, Column: 48, Reason: no viable alternative at input 'in'
\end{lstlisting}

After testing a lot of things, we found out that \emph{in} is a reserved keyword in metadepth. So we had to change \emph{in} in our code to something like \emph{incoming}.

\subsection{Schedule model}

\emph{The schedule model can be found in schedules.mdepth}\\

Implementing the \emph{Schedule} model itself wasn't really a big issue.
Again we started from an abstract node -- in this case \emph{Step} -- which was then extended by the \emph{RegularStep}, \emph{EndStep} and \emph{StartStep} nodes.

Adding constrains for this model, and combining them with the \emph{Railway} model was not trivial. We had various problems accessing the members from nodes from the other model. We tried with separate models and nested models, but both had problems.

One of the errors we've seen a lot:

\begin{lstlisting}
> loading ./schedule.mdepth (5 clabjects created in 0.011 s).
:: loading ./railway.mdepth (24 clabjects created in 0.283 s).
> :: Parsing error in constraint uniqueschedule, Type 'Schedule' not found (1:48)
\end{lstlisting}

See the section below for more info about how we solved this.

\subsection{Combining the models}

After having created those two models, we needed to find a way to use them together. We tried many things, but metadepth lacked support for most of them.
Luckily we had an explanation during the seminar session, where we learnt that we had to combine them in a new model and write the mixed constraints there.\\

\emph{This combined model can be found in the metamodel.mdepth}.



\section{Operational Semantics}

\subsection{Types}

Since we didn't find any casting mechanisms in EOL, we had to find another solution to derive the concrete objects from the abstract base classes like \emph{Segment}. Fortunately EOL supports polymorphism with runtime dispatch. This made it possible to define a \emph{getNext} function for each sub type:

\begin{lstlisting}
// railwaySimulate.eol

operation Straight getNext() : Segment {
	return self.outgoing;
}

operation Turnout getNext() : Segment {
	if (self.straight) {
		return self.outgoing[0];
	}
	else {
		return self.outgoing[1];
	}
}

operation Junction getNext() : Segment {
	return self.outgoing;
}

operation Station getNext() : Segment {
	// some magic to check if it's an End or Start station
	return self.outgoing;
}
\end{lstlisting}


Another way to distinguish types is function overloading. This turns out useful when we don't want to implement the polymorphic function from above for each object, but just distinguish one derivated class from all the others.

\begin{lstlisting}
// railwaySimulate.eol

operation isEndStation(trainSteps : Map, t : String, s : Segment) : boolean {
	return false;
}

operation isEndStation(trainSteps : Map, t : String, station : Station) : boolean {
	trainSteps.get(t).station.println();
	if (trainSteps.get(t).station = station.name and station.outgoing.size() = 0) {
		return true;
	}
	else {
		return false;
	}
}
\end{lstlisting}


\subsection{Simulation}

A simulation is started in the \emph{main} function, which initializes the collection types for the state of the railway simulation.
The \emph{main} function then calls the \emph{initialStep} function.
When everything is initialized, a while loops calls the  \emph{step} function until all trains have reached their destination.

\begin{lstlisting}
// railwaySimulate.eol

operation main() {
	"Starting Simulation".println();
	"-------------------".println();
	
	var segments : Sequence(Segment);
	segments = Segment.allInstances();
	
	var trainLocations : Map(String, Segment);
	var trainSteps : Map(String, RegularStep);
	initialStep(trainLocations, trainSteps, segments);
	
	while(not trainLocations.isEmpty()) {
		step(trainLocations, trainSteps);
	}
	
	"------------------".println();
	"Ending Simulation".println();
}
\end{lstlisting}


\subsubsection{Initial step}

The initial step puts all trains in their start station (according to their start step) and checks for violations.

\begin{lstlisting}
// railwaySimulate.eol

operation initialStep(trainLocations : Map, trainSteps : Map, segments : Sequence) {
	for (s in Schedule.allInstances()) {
		// get train
		var train : Train;
		train = Train.allInstances().select(t | t.name = s.trainName).first();
		
		//	set train in station
		//	-> this is the first step in the schedule
		var start : Segment;
		start = Station.allInstances().select(t | t.name = s.start.station).first();
		var startStation = segments.select(seg | seg = start).first();



		// check if there is already a train on that segment
		if (trainLocations.containsValue(startStation)) {
			throw ("VIOLATION | There is already a train on this segment! Trying to add train " + train + " to " + startStation);
		}

		trainLocations.put(train.name, startStation);
		trainSteps.put(train.name, s.start);

		(train.name + " | started at " + startStation.segId + ".").println();
	}
}
\end{lstlisting}



\subsubsection{Regular step}

The \emph{step} function is the most complicated function, since it handles all events of the controller.
At first it checks for the end step (cfr. paragraph \emph{End Step}).\\

If the next segment of the train is a junction or a turnout, we need to set the corresponding \emph{straight} value.

\begin{lstlisting}
// railwaySimulate.eol

//	if it is a turnout or a junction -> set direction
if (Turnout.allInstances().count(trainLocations.get(train).getNext()) + Junction.allInstances().count(trainLocations.get(train).getNext()) > 0) {
	if ( changedSegments.includes(trainLocations.get(train).getNext()) ) {
		// Only change it one time per iteration
	}
	else {
		if(trainLocations.values().includes(trainLocations.get(train).getNext())) {
			// there is already a train on the junction, so we have to wait.
		}
		else {
			//"ON A SPECIAL SEGMENT!".println();
			trainLocations.get(train).getNext().straight = trainSteps.get(train).straight;
			("        Changed straight to " + trainLocations.get(train).getNext().straight + " on segment " + trainLocations.get(train).getNext().segId).println();
			changedSegments.add(trainLocations.get(train).getNext());
		}
	}
}
\end{lstlisting}

If the junctions and turnouts are handled, we check for the lights:

\begin{lstlisting}
// railwaySimulate.eol

// set light
// If no train is present on the segment the train wants to move to, it sets the light of the segment the train is currently on to green.
var nextSegment = Segment;
nextSegment = trainLocations.get(train).getNext();
if(trainLocations.values().includes(nextSegment)) {
	// train on the next segment
	trainLocations.get(train).light.color = "RED";
}
else {
	// no train on the next segment
	if(Junction.allInstances().count(nextSegment) > 0) {
		// Junction case! 
		// We need to check whether or not the mode is correct for us
		if(nextSegment.straight = trainSteps.get(train).straight) {
			// mode is equal to ours, green light!
			trainLocations.get(train).light.color = "GREEN";	
		}
	}
	else {
		trainLocations.get(train).light.color = "GREEN";	
	}
}
\end{lstlisting}

The last part of the function loops over all trains and update their location (if they are allowed to move):

\begin{lstlisting}
// railwaySimulate.eol

for (train in trains) {
	if ("GREEN" = trainLocations.get(train).light.color) {
		("        GREEN light on segment " + trainLocations.get(train).segId).println();
		// Update location
		var currentSegment = trainLocations.get(train);
		var nextSegment = currentSegment.getNext();
		// Check for collisions before moving to the segment
		if (trainLocations.containsValue(nextSegment)) {
			throw ("VIOLATION | There is already a train on this segment! Trying to add train " + train + " to " + nextSegment);
		}
		trainLocations.put(train, nextSegment);
		(train + " | moved forward past the green light from segment " + currentSegment.segId + " to segment " + nextSegment.segId).println();
		
		// Move to next step in schedule
		trainSteps.put(train, trainSteps.get(train).outgoing);
	}
	else {
		// ELSE RED, DON'T MOVE
		("        RED light on segment " + trainLocations.get(train).segId).println();
	}
}
\end{lstlisting}


\subsubsection{End step}

The end step is handled in the \emph{step} function. Before every regular step, we check if the train reached it's end station:

\begin{lstlisting}
// railwaySimulate.eol

if (isEndStation(trainSteps, train, segmentOfTrain)) {
	(train + " | arrived at " + segmentOfTrain.segId + ".").println();
	trainSteps.remove(train);
	trainLocations.remove(train);
	return;
}
\end{lstlisting}

In the requirements it is said that \textit{"The 'end' step contains the name of the station where the train will stop.}. However, a little bit further it is said that \textit{It is an error if there is no step (the schedule has reached the end) when a train wants to leave a turnout.}. We assumed that an end step can only be in an end station and not on every segment.


\subsubsection{Trace}

Sample trace:
{
\tiny
\begin{lstlisting}
Starting Simulation
-------------------
M1234 | started at station:gent.
K1234 | started at station:antwerpen.
        GREEN light on segment station:antwerpen
K1234 | moved forward past the green light from segment station:antwerpen to segment straight:r1
        GREEN light on segment station:gent
M1234 | moved forward past the green light from segment station:gent to segment straight:r2
        Changed straight to true on segment junction:junction1
        GREEN light on segment straight:r1
K1234 | moved forward past the green light from segment straight:r1 to segment junction:junction1
        RED light on segment straight:r2
        GREEN light on segment junction:junction1
K1234 | moved forward past the green light from segment junction:junction1 to segment straight:r4
        RED light on segment straight:r2
        Changed straight to false on segment junction:junction1
        GREEN light on segment straight:r4
K1234 | moved forward past the green light from segment straight:r4 to segment station:berchem
        GREEN light on segment straight:r2
M1234 | moved forward past the green light from segment straight:r2 to segment junction:junction1
K1234 | arrived at station:berchem.
        GREEN light on segment junction:junction1
M1234 | moved forward past the green light from segment junction:junction1 to segment straight:r4
        GREEN light on segment straight:r4
M1234 | moved forward past the green light from segment straight:r4 to segment station:berchem
M1234 | arrived at station:berchem.
------------------
Ending Simulation

\end{lstlisting}
}

\section{Executing the code}

The jar file of metadepth is added to this repo. We also created an actions file which you can pipe to metadepth. It executes \emph{instance6} by default.

\begin{lstlisting}
$ java -jar metaDepth.jar < actions 
\end{lstlisting}

\section{Test instances}
We made a few instance files that show the functionality.
\subsection{instance.mdepth}
Straight from Antwerpen to Berchem. No junctions or turnouts.
\subsection{instance2.mdepth}
Straight from Antwerpen to Berchem. Going straight over a turnout.
\subsection{instance3.mdepth}
From Hasselt to Antwerp. Take turnout.
\subsection{instance4.mdepth}
Two trains and a junction. Arrive at the junction at different times.
\subsection{instance5.mdepth}
Simulation with invalid start states.
\subsection{instance6.mdepth}
Two trains and a junction. Both trains want to enter the junction at the same time so one of them has to wait. 
In the constraints it is said that you have to work with a random number generator and seeds. We did not do this as we did not easily find a way to get random generated numbers in eol. The first one in the Trains list is allowed on the junction first. 
\end{document}